import * as React from 'react';

class FormPreview extends React.Component<any, any> {
    constructor(props) {
        super(props);        
    }

    render() {
        //temporay use localStore waiting to apply redux
        let form = localStorage.getItem('previewForm') || '';
        localStorage.removeItem('previewForm');
        return (
            <div >
                <br />
                <div dangerouslySetInnerHTML={{ __html: form }} className="container" />
            </div>
        );
    }
}

export default FormPreview;
