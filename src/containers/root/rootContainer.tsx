import * as React from 'react';
import FormBuilder from '../formBuilder';
import { connect } from 'react-redux';

class RootContainer extends React.Component<any, any> {
	render() {
		return (
			<div className="site-wrapper content">
					{/* <div className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h2>Welcome to React</h2>
          </div>*/}
					<br />
					<br />
					<FormBuilder />
			</div>
		);
	}
}

export default connect(null, null)(RootContainer);