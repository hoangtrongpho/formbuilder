import * as React from 'react';
import RootContainer from './rootContainer';
import FormPreview from '../preview/formPreview';
import Debug from '../../shared/config/debug'

import { HashRouter as Router, Switch, Route } from 'react-router-dom';

if (location.href.indexOf("localhost:3000") !== -1)
	Debug.init();

class App extends React.Component {
	render() {
		return (
			<Router>
				<Switch>
					<Route exact path="/" component={RootContainer} />
					<Route path="/preview" component={FormPreview} />
				</Switch>
			</Router>
		);
	}
}

export default App;
