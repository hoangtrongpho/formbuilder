import * as React from 'react';
// import ReactDOM from 'react-dom';
// import * as Immutable from 'seamless-immutable'
// import { DragDropContext } from 'react-beautiful-dnd';
import { StagePanel } from '../../components/stagePanel';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

// class FormBuilder extends React.Component<any, { sourceItems: Immutable.ImmutableArray<FormMenuItem>; destItems: Array<any> }> {  
class FormBuilder extends React.Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   sourceItems: Immutable([ //getItems(10),
    //     { type: StageItemType.TEXT_FIELD, label: "Text Field" },
    //     { type: StageItemType.PHONE_NUMBER, label: "Phone Number" },
    //     { type: StageItemType.EMAIL, label: "Email" },
    //     { type: StageItemType.COLUMN, label: "Column" },
    //     { type: StageItemType.PANEL, label: "Panel" },
    //   ]),
    //   destItems: []
    // };
    // this.onDragEnd = this.onDragEnd.bind(this);
  }

  // onDragEnd(result) {
  //   // dropped outside the list
  //   if (!result.destination) {
  //     return;
  //   }

  //   if (result.destination.droppableId === "stage") {
  //     if (result.source.droppableId === "stage") {
  //       this.formStage.reOrderItem(
  //           result.source.index,
  //           result.destination.index
  //         );
  //     } else {
  //       let item = this.state.sourceItems.find( (value, index, obj) => {
  //         if (index === result.source.index)
  //           return value;
  //       });
  //       this.formStage.insertItem( { type: item.id }, result.destination.index );
  //     }   
  //   }
  // }

  render() {
    return (
      <StagePanel />
    );
  }
}

export default DragDropContext(HTML5Backend)(FormBuilder);