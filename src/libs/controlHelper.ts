import { ZoneItem, StageItem } from '../components/stagePanel';
import { StageItemType, LabelPosition } from '../components/stagePanel/model/constants';

class ControlHelper {
	static indentSize = 4;
	static renderFormHtml(zone: ZoneItem) {
		let html = '';
		zone.children.forEach((item: StageItem) => {
			html += this.renderHtml(item, 4);
		});

		html = '<form id="rendered-form">\n' + html + '</form>';
		return html;
	}

	static renderHtml(item: StageItem, indentSize?: number) {
		let indent = indentSize ? indentSize : 0;
		let res = appendSpace('', indent);
		let firstStyle = item.properties.columnCssFirst ? (item.properties.columnCssFirst.baseStyle ? item.properties.columnCssFirst.baseStyle : "") : "";
		let firstClass = item.properties.columnCssFirst ? (item.properties.columnCssFirst.baseClass ? item.properties.columnCssFirst.baseClass : "") : "";
		let secondStyle = item.properties.columnCssSecond ? (item.properties.columnCssSecond.baseStyle ? item.properties.columnCssSecond.baseStyle : "") : "";
		let secondClass = item.properties.columnCssSecond ? (item.properties.columnCssSecond.baseClass ? item.properties.columnCssSecond.baseClass : "") : "";
		let panelStyle = item.properties.panelCss ? (item.properties.panelCss.baseStyle ? item.properties.panelCss.baseStyle : "") : "";
		let panelClass = item.properties.panelCss ? (item.properties.panelCss.baseClass ? item.properties.panelCss.baseClass : "") : "";
		if (!item.isContainer) {
			res += '<div class="form-group row">\n';
			res += appendSpace('', indent + ControlHelper.indentSize);	
			res += renderControl(item, indent);
			res = appendSpace(res, indent) + '</div>\n';
		} else {
			res += '<div class="row">\n';
			if(item.type=='panel')
			{
				item['zones'].forEach((z: ZoneItem,index) => {
					res = appendSpace(res, indent + ControlHelper.indentSize);
						res += `<div style="${panelStyle}" class="${panelClass}">\n`;

					z.children.forEach((child: StageItem) => {
						res += this.renderHtml(child, indent + 2 * ControlHelper.indentSize);
					});
					res = appendSpace(res, indent + ControlHelper.indentSize) + '</div>\n';
				});
				res = appendSpace(res, indent) + '</div>\n';
			}else{
				item['zones'].forEach((z: ZoneItem,index) => {
					res = appendSpace(res, indent + ControlHelper.indentSize);
					if(index == 0)
						res += `<div style="${firstStyle}" class="${firstClass}">\n`;
					if(index == 1)
						res += `<div style="${secondStyle}" class="${secondClass}">\n`;

					z.children.forEach((child: StageItem) => {
						res += this.renderHtml(child, indent + 2 * ControlHelper.indentSize);
					});
					res = appendSpace(res, indent + ControlHelper.indentSize) + '</div>\n';
				});
				res = appendSpace(res, indent) + '</div>\n';
			}
				
		}

		return res;
	}
}

const appendSpace = (str, indent) => {
	for (var i = 0; i < indent; i++) {
		str += ' ';
	}
	return str;
};

const renderControl = (item: StageItem, indent: number) => {
	let res = '';
	let _class = item.properties.labelCss ? (item.properties.labelCss.baseClass?item.properties.labelCss.baseClass:""): "";
	let _style = item.properties.labelCss ? (item.properties.labelCss.baseStyle?item.properties.labelCss.baseStyle:""): "";

	switch (item.properties.labelPosition) {
		case LabelPosition.TOP:
			res += `<label style="${_style}" class="control-label ${_class}" for="${item.id}">${item.properties.label}</label>\n`;
			res += appendSpace('', indent + ControlHelper.indentSize);
			res += switchControl(item);
			break;
		case LabelPosition.BOTTOM:
			res += switchControl(item);
			res += appendSpace('', indent + ControlHelper.indentSize);
			res += `<label style="${_style}" class="control-label ${_class}" for="${item.id}">${item.properties.label}</label>\n`;
			break;
		case LabelPosition.LEFT:
			res += `<label style="${_style}" class="control-label col-sm-2 col-form-label ${_class}" for="${item.id}">${item.properties.label}</label>\n`;
			res += appendSpace('', indent + ControlHelper.indentSize);
			res += '<div class="col-sm-10">\n';
			res += appendSpace('', 2 * indent + ControlHelper.indentSize);
			res += switchControl(item);
			res += appendSpace('', indent + ControlHelper.indentSize);    
			res += '</div>\n';
			break;
		case LabelPosition.RIGHT:
			res += '<div class="col-sm-10">\n';
			res += appendSpace('', 2 * indent + ControlHelper.indentSize);
			res += switchControl(item);
			res += appendSpace('', indent + ControlHelper.indentSize);    
			res += '</div>\n';
			res += appendSpace('', indent + ControlHelper.indentSize);
			res += `<label style="${_style}" class="control-label col-sm-2 col-form-label ${_class}" for="${item.id}">${item.properties.label}</label>\n`;
			break;
	}
	
	if(item.type === StageItemType.HTML){
		res = switchControl(item) + '\n';
	}

	return res;
};

const switchControl = (item: StageItem) => {
	let res = '';	
	let _controlClass = item.properties.controlCss ? (item.properties.controlCss.baseClass? item.properties.controlCss.baseClass:""): "";
	let _controlStyle = item.properties.controlCss ? (item.properties.controlCss.baseStyle? item.properties.controlCss.baseStyle:""): "";
	switch (item.type) {
		case StageItemType.VIEWPROOF:
			res += `<button type="button" id="${item.id}"  class="btn btn-primary">View Proof</button>\n`;
			break;
		case StageItemType.XMPIE:
			if(item.data.imageUrl!="") 
				res += `<img id="${item.id}" style="width: 100%" src="${item.data.imageUrl}"/>\n`;
			else 
				//res += `<label id="${item.id}" class="btn btn-warning">XMPie</label>\n`;
				res += `<img id="${item.id}" style="width: 100%" src="${item.data.DefaultImage}"/>\n`;
			break;
		case StageItemType.QUANTITY:
			res += `<input class="form-control ${_controlClass}" style="${_controlStyle}" type="text" id="${item.id}" />\n`;
			break;
		case StageItemType.ADDTOCART:
			res += `<button type="button" id="${item.id}" class="${_controlClass}" style="${_controlStyle}">Add To Cart</button>\n`;
			break;
		case StageItemType.TEXT_FIELD:
			if(item.properties.textboxFormat=="phoneNumberFirst")
				res += `<input class="form-control ${_controlClass}" style="${_controlStyle}" placeholder="(###) ###-####" type="text" id="${item.id}" />\n`;
			else if(item.properties.textboxFormat=="phoneNumberSecond")
				res += `<input class="form-control ${_controlClass}" style="${_controlStyle}" placeholder="###.###.####" type="text" id="${item.id}" />\n`;
			else if(item.properties.textboxFormat=="email")
				res += `<input class="form-control ${_controlClass}" style="${_controlStyle}" placeholder="Email" type="text" id="${item.id}" />\n`;
			else
				res += `<input class="form-control ${_controlClass}" style="${_controlStyle}" type="text" id="${item.id}" />\n`;
			break;
		case StageItemType.DROPDOWN:
			res += initDropdown(item,_controlClass,_controlStyle);
			break; 
		case StageItemType.HTML:
			res = item.properties.html || '';
			break;
	}
	return res;
};
const initDropdown = (data,_controlClass,_controlStyle) => {
	debugger;
	let res =``;
	res += `<select class="form-control ${_controlClass}" style="${_controlStyle}">`;
	for(let i=0;i<data.properties.dropdownData.length;i++){
		res += `<option value="${data.properties.dropdownData[i].value}">${data.properties.dropdownData[i].item}</option>`;
	}
	res += `</select>\n`;
	return res;
}

export default ControlHelper;
