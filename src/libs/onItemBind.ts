import { ZoneItem, StageItem, ContainerItem } from '../components/stagePanel';
let itemCount = 0; 
let itemType="";
class OnItemBind {

	static getLabelName(item: StageItem, zone: ZoneItem) {
        itemCount = 0;
        itemType = item.type;
        this._findXMPieStageItem(zone);
        let finalName= item.type +" "+ (itemCount+1);
		return finalName;
	}
    //find all item control in ZONE
    static _findXMPieStageItem(zone: ZoneItem,container?: ContainerItem): null | { item: StageItem; index: number; zone: ZoneItem; container?: ContainerItem }{

        for (let i = 0; i < zone.children.length; i++) {
            //debugger;
            let item = zone.children[i] as ContainerItem;
            if (item.isContainer) {
                if (item.zones && item.zones.length){
                    for (let j = 0; j < item.zones.length; j++) {
                        this._findXMPieStageItem(item.zones[j], item);
                        //if (result) return result;
                    }
                }
            }
            //let itemList = Array<ContainerItem>();
            if (item.type === itemType){
                itemCount++;
            }
            if (i === zone.children.length-1) {
                //alert("itemList:"+JSON.stringify(itemList,null,2));
                return { item, index: i, zone, container };
            }
            
        }
        return null;
    }
}

export default OnItemBind;
