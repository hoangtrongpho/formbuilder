import ApiExecuter from "../shared/common/apiExecuter";

//const appBaseUrl = "http://localhost:25000/Plugins/CC.Plugins.FormBuilder/";
const appBaseUrl = "https://nationaldev.lcpstore.com/Plugins/CC.Plugins.FormBuilder/";
// const appXMPieUrl = "http://cc.lakecountypress.com/DesktopModules/XMPieProof/API/XMPie/";

const instance = () => {
    return {  
        saveForm: (data:{productId:number, jsonContent:any}) => apiCommon.post('Main/SaveFormBuilder', data),
        loadForm: (data:{productId:number}) => apiCommon.get('Main/LoadFormBuilder', data),
        submitJob:(documentId:string) => apiCommon.get('XMPie/SubmitXMPieCC', {documentId}),
        getJobStatus:(jobId:string) => apiCommon.get('XMPie/GetJobStatusCC', {jobId}),
        getConnections:() => apiCommon.get('Main/GetConnections'),
        runDropdownQuery: (data:{sql:string, connection:string}) => apiCommon.post('Main/RunDropdownQuery', data),
        setToken: apiCommon.setToken
    }
}

const fetchRefreshToken = () => {
    // do nothing now.    
    return new Promise<any>((resolve) => { alert ("Unauthorized. Haven't implemented OAuth yet"); });
}

const apiCommon = new ApiExecuter(appBaseUrl, fetchRefreshToken);

export default instance();