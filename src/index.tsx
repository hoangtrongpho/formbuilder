import * as React from 'react';
import * as ReactDOM from 'react-dom'
import App from './containers/root/App';
import registerServiceWorker from './registerServiceWorker';
import {  } from 'react-router-redux';
import { Provider } from 'react-redux';
import createStore, {  } from './shared/redux';
import './index.css';
// create our store
const store = createStore();
const target = document.querySelector('#root');

ReactDOM.render(
  <Provider store={store}>
    {/* <ConnectedRouter history={history}> */}
        <App />
    {/* </ConnectedRouter> */}
  </Provider>,
  target
);

registerServiceWorker();
