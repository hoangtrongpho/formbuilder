import * as React from 'react';
import ModalPanel from './modalPanel';
import { ModelItem } from './modalPanel';

const defaultValue = {
	hideHeader: false,
	closeText: 'OK',
	closeStyle: 'default',
	saveStyle: 'primary',
	saveText: 'Yes'
}

class Dialog extends React.Component<ModelItem, { show: boolean; message: string; options: ModelItem }> {
	constructor(props, context) {
		super(props);
		this.state = {
			show: false,
			message: '',
			options: defaultValue
		};
	}

	show(msg: any, options: ModelItem) {
		this.setState({
			show: true,
			message: msg,
			options: {
				...defaultValue,
				closeText: 'Close',
				hideHeader: true,
				...options
			}
		});
	}

	info(msg: any, options: ModelItem) {
		this.setState({
			show: true,
			message: msg,
			options: {
				...defaultValue,
				closeText: 'OK',
				closeStyle: 'info',
				hideHeader: true,
				...options
			}
		});
	}

	success(msg: any, options: ModelItem) {
		this.setState({
			show: true,
			message: msg,
			options: {
				...defaultValue,
				closeText: 'OK',
				closeStyle: 'success',
				hideHeader: true,
				...options
			}
		});
	}

	error(msg: any, options: ModelItem) {
		this.setState({
			show: true,
			message: msg,
			options: {
				...defaultValue,
				closeText: 'OK',
				closeStyle: 'danger',
				hideHeader: true,
				...options
			}
		});
	}

	confirm(msg: any, options: ModelItem) {
		this.setState({
			show: true,
			message: msg,
			options: {
				...defaultValue,
				closeText: 'No',
				saveText: 'Yes',
				title: 'Warning!',
				...options
			}
		});
	}

	close() {
		this.setState({ show: false, options: { ...this.state.options, save: undefined } });
	}

	save() {
		if (this.state.options.save) this.state.options.save();
		this.close();
	}

	render() {
		return (
			<ModalPanel
				{...this.state.options}
				show={this.state.show}
				close={this.close.bind(this)}
				save={this.state.options.save ? this.save.bind(this) : null}
			>
				<div>{this.state.message}</div>
			</ModalPanel>
		);
	}
}

export default Dialog;
