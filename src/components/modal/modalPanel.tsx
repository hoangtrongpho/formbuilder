import * as React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { Form } from 'react-form';

class ModalPanel extends React.Component<ModalControlProps, any> {
	constructor(props, context) {
		super(props);
	}

	render() {
		let { size, formValue } = this.props;
		return (
			<Modal show={this.props.show} onHide={this.props.close} bsSize={size} dialogClassName={this.props.dialogClass}>
				{this.props.hideHeader ? null : (
					<Modal.Header closeButton>
						<Modal.Title>{this.props.title} {this.props.curTab}</Modal.Title>
					</Modal.Header>
				)}
				<Form onSubmit={submittedValues => { this.props.handleSubmit(submittedValues); }} defaultValues={formValue} validateError={this.props.errorValidator}>
					{formApi => (
						<form onSubmit={formApi.submitForm}>
							<Modal.Body>{this.props.saveButton ? React.cloneElement(this.props.children as React.ReactElement<any>, {
								formApi: formApi
							}) : this.props.children}</Modal.Body>
							{
								(this.props.curTab!="dropdownData")?
								<Modal.Footer>
									<Button onClick={() => this.props.close()} bsStyle={this.props.closeStyle}>
										{this.props.closeText || 'Close'}
									</Button>
									{this.props.save && !this.props.saveButton? (
										<Button onClick={this.props.save} bsStyle={this.props.saveStyle}>
											{this.props.saveText || 'Save'}
										</Button>
									) : null}
									{this.props.saveButton ? this.props.saveButton : null}
								</Modal.Footer>
								:null
							}
							
						</form>
					)}
				</Form>
			</Modal>
		);
	}
}

export interface ModalControlProps extends React.Props<any>, ModelItem {
	close: Function;
	save?: any;
	dialogClass?: string;
	saveButton?: any;
	handleSubmit?: any;
	formValue?: any;
	errorValidator?: any;
	curTab?:string;
}

export interface ModelItem {
	show?: boolean;
	title?: string;
	saveText?: string;
	saveStyle?: string;
	hideHeader?: boolean;
	closeText?: string;
	closeStyle?: string;
	save?: Function;
	size?: "xs" | "xsmall" | "sm" | "small" | "medium" | "lg" | "large" | undefined;
}

export default ModalPanel;
