import { createActions, createReducer, createTypes } from 'reduxsauce'
import * as Immutable from 'seamless-immutable'
// import { AppState } from '../../typings/appTypes'


/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  showEditControl: ['show']
})

export const StartupTypes = createTypes(`
  ON_SHOW_MODAL
`)
export default Creators

export const INITIAL_STATE = Immutable({    
  showEditControl: false
})

export const showEditControl = (state, { show }: {show:any}) => {
  return state.merge({ showEditControl: show })
}

export const reducer = createReducer(INITIAL_STATE, {
   [Types.SHOW_EDIT_CONTROL]: showEditControl
})
