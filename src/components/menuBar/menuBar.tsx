import * as React from 'react';

class MenuBar extends React.Component<MenuBarPropsType, any> {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="btn-toolbar pull-right" role="toolbar">
				<div className="btn-group">
					<button className="btn btn-primary button-import" onClick={this.props.onSave}>
						<span className="glyphicon glyphicon-save"></span>
						Save
					</button>
					{/* <button className="btn btn-primary button-import" onClick={this.props.onLoad}>
						<span className="glyphicon glyphicon-import"></span>
						Load
					</button> */}					
				</div>
				<div className="btn-group">					
					<button className="btn btn-default button-import" onClick={this.props.onImport}>
						<span className="glyphicon glyphicon-import"></span>
						Import
						</button>
					<button className="btn btn-default button-export" onClick={this.props.onExport}>
						<span className="glyphicon glyphicon-export"></span>
						Export
					</button> 
				</div>
				<div className="btn-group">
					<button className="btn btn-default button-code" onClick={this.props.onCodeView}>
						<span className="glyphicon glyphicon-align-center"></span>
						Code
						</button>
				</div>
				<div className="btn-group">
					<button className="btn btn-success button-preview" onClick={this.props.onPreview}>
						Preview
						</button>
				</div>
			</div>
		);
	}
}

export default MenuBar;

interface MenuBarPropsType extends React.Props<any> {
	onImport: () => void;
	onExport: () => void;
	onCodeView: () => void;
	onPreview: () => void;
	onSave: () => void;
	// onLoad: () => void;
}