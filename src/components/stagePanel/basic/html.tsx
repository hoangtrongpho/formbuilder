import * as React from 'react';
import { CommonBasicProps } from './'
import { StageItem } from '../index';
import BaseElement from './_base';

const renderChild = (item: StageItem) => {
    return (
        <div className="prev-holder">
            <div className="form-group">
                <textarea disabled className="form-control" rows={5} style={{overflow:'auto', resize:'none'}} value={item.properties.html}>
                </textarea>
            </div>
        </div>
    );
}

class HTML extends React.Component<HTMLProps, any> {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <BaseElement {...this.props} >
                {renderChild(this.props.item)}
            </BaseElement>
        );
    }
}

export default HTML;

export interface HTMLProps extends CommonBasicProps {
}