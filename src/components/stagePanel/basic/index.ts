import TextField from './textField';
import HTML from './html';
import { StageItem } from '../';
import { ContainerEvents } from '../container';

export { 
    TextField,
    HTML 
}

export interface CommonBasicProps {
    item:StageItem;
    index:number;
    events:ContainerEvents;
}
