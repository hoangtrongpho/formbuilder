import { CommonBasicProps } from './'
import BaseElement from './_base';
import * as React from 'react';
import { StageItem } from '../index';
import getStyle from './convertStyle';

const renderChild = (item: StageItem,inputStyle: any,newClass: any) => {
    return (
        <div className="prev-holder">
            <div className="form-group">
                {
                    (item.properties.textboxFormat=="phoneNumberFirst") ? <input style={inputStyle} placeholder="(###) ###-####" className={newClass} type="text" /> :
                    (item.properties.textboxFormat=="phoneNumberSecond") ? <input style={inputStyle} placeholder="###.###.####" className={newClass} type="text" /> : 
                    (item.properties.textboxFormat=="email") ? <input style={inputStyle} placeholder="Email" className={newClass} type="text" /> :
                    <input style={inputStyle} className={newClass} type="text" />
                }
            </div>
        </div>
    );
}

class TextField extends React.Component<TextFieldProps, any> {
    constructor(props) {
        super(props);
    }

    render() {
        let newClass = this.props.item.properties.controlCss ? (this.props.item.properties.controlCss.baseClass ? this.props.item.properties.controlCss.baseClass : "form-control") : "form-control";
        let newStyle = this.props.item.properties.controlCss ? (this.props.item.properties.controlCss.baseStyle ? this.props.item.properties.controlCss.baseStyle : "") : "";
		let styleobj={};
		if(newStyle != "")
			styleobj = getStyle(newStyle);
        return (
            <BaseElement {...this.props} >
                {renderChild(this.props.item,styleobj,newClass)}
            </BaseElement>
        );
    }
}

export default TextField;

export interface TextFieldProps extends CommonBasicProps {
}