const grid = 8;
export const getItemStyle = (draggableStyle, isDragging) => {
    const style = {
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: grid * 2,
    // marginBottom: grid,

    // change background colour if dragging
    background: isDragging ? 'lightgreen' : 'grey',

    // styles we need to apply on draggables
    ...draggableStyle,
    }

    // console.log(JSON.stringify(style, null, 2))
    return style;
};