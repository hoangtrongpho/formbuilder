const getStyle = (item) => {
    let sumObj = Array();
        if(item != ""){
            let styleArr = item.split(";");
            if(styleArr.length >0 ){
                for(let i=0;i<styleArr.length;i++)
                {
                    let detailArr = styleArr[i].split(":");
                    let obj = {
                        name : detailArr[0],
                        value : detailArr[1]
                    };
                    sumObj.push(obj);
                }
            }
        }
        return transferToReactStyle(sumObj);
}

const transferToReactStyle = (item) => {
    let sumObj={};
    if(item.length >0){
        for(let i=0;i<item.length;i++){
            let _name = "";
            if(item[i].name.includes("-")){
                let cssArr = item[i].name.split("-");
                _name = cssArr[0]+capitalizeFirstLetter(cssArr[1]);
            }else{
                _name = item[i].name;
            }

            if(_name != ""){
                    sumObj[_name]=item[i].value;
            }
        }
    }
    return sumObj;
}

const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
export default getStyle;