import * as React from 'react';
// import { StageItem } from '../';
import StageControl, { StageControlProps } from '../stageControl';

class BaseElement extends React.Component<StageControlProps, any> {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <StageControl {...this.props}>
                {this.props.children}
            </StageControl>
        );
    }
}

export default BaseElement;