import * as React from 'react';
import ModalPanel, { ModalControlProps } from '../../modal/modalPanel';
import StageTabControl from './stageTabControl';
import { StageItem,ZoneItem } from '../index';
import { VisibilityRuleType } from '../model/constants';

const errorValidator = (values) => {

  let javascriptMessage;
  if (values.visibility.ruleType === VisibilityRuleType.Javascript) {
    try {
      let fn = eval('(' + values.visibility.javascript + ')');
      javascriptMessage = fn() === true || fn() === false ? null : 'Javascript must be a function'
    } catch (error) {
      javascriptMessage = 'Javascript must be a function';
    }
  }

  return {
    javascript: javascriptMessage ? javascriptMessage : null
  };
};


class ModalEditControl extends React.Component<EditModelProps, any> {
  constructor(props, context) {
    super(props);
    this.state = {
      item: this.props.item || {},
      rootZone: this.props.rootZone
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }
  render() {
    return (
      <ModalPanel
        errorValidator={errorValidator}
        formValue={this.state.item}
        handleSubmit={this.handleSubmit}
        dialogClass={'edit-control-modal'}
        size={'large'}
        title={'Edit control'}
        {...this.props}
        closeText={'Cancel'}
        closeStyle={'danger'} >
        <StageTabControl item={this.state.item}  rootZone = {this.state.rootZone} handleSubmit={this.props.handleSubmit} checkTab={this.props.checkTab} closeModal={this.props.close}/>
      </ModalPanel>
    )
  }
  handleSubmit(form) {
    debugger
    this.props.handleSubmit(form);
  }
 
}

export interface EditModelProps extends ModalControlProps {
  item: StageItem;
  rootZone: ZoneItem;
  handleSubmit: Function;
  checkTab: Function;
  curTab:string;
}
export default ModalEditControl;