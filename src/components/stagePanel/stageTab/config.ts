const config = {
	properties: {
		tabs: [ 'textField', 'column', 'panel', 'html', 'quantity','addtocart','dropdown' ],
		title: 'Properties',
		nolabel: [ 'column', 'panel' ]
	},
	validation: {
		tabs: [ 'textField', 'column', 'panel', 'html' ],
		title: 'Validations'
	},
	visibility:{
		tabs: [ 'textField', 'column', 'panel', 'html', 'column', 'panel' ],
		title: 'Visibility'
	},
	textbox:{
		tabs: [ 'textField'],
		title: 'Textbox'
	},
	style:{
		tabs: [ 'textField', 'column', 'panel', 'quantity', 'addtocart','dropdown'],
		title: 'Style'
	},
	dropdownData:{
		tabs: ['dropdown'],
		title: 'Dropdown Data'
	},
	html:{
		tabs: [ 'html' ],
		title: 'HTML'
	},
	
};

export default config;
