import * as React from 'react';
import { FormField } from 'react-form';
import AceEditor from 'react-ace';
import 'brace/mode/html';
import 'brace/theme/github';

// Define a custom message component
const Message = ({ color, message }) => {
	return (
		<div className="mb-4" style={{ color }}>
			<small>{message}</small>
		</div>
	);
}

class CustomTextAreaWrapper extends React.Component<any, any> {
	render() {
		const {
        	fieldApi,
			onChange,
			...rest
      	} = this.props;

		const {
        getValue,
			getError,
			getWarning,
			getSuccess,
			setValue,
			setTouched
      	} = fieldApi;

		const error = getError();
		const warning = getWarning();
		const success = getSuccess();
debugger
		return (
			<div>
				{error ? <Message color="red" message={error} /> : null}
				{!error && warning ? <Message color="orange" message={warning} /> : null}
				{!error && !warning && success ? <Message color="green" message={success} /> : null}
				<AceEditor
					height={'420px'}
					width={'100%'}
					theme="github"
					editorProps={{ $blockScrolling: true }}
					value={getValue()}
					setOptions={{
						enableBasicAutocompletion: true,
						// enableLiveAutocompletion: true,
						enableSnippets: true,
						showLineNumbers: true,
						tabSize: 2,
					}}
					// showPrintMargin={true}
					// showGutter={true}
					// highlightActiveLine={true}
					onChange={(e) => {
						setValue(e);
						if (onChange) {
							onChange(e);
						}
					}}
					onBlur={()=>{
						setTouched();
					}}
					{...rest}
				/>				
			</div>
		);
	}
}

const CustomTextArea = FormField(CustomTextAreaWrapper);
export default CustomTextArea;

