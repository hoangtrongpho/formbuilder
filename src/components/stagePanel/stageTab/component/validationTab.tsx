import * as React from 'react';
import { Tab } from 'react-bootstrap';
import { TabItem } from './';

class ValidatioinTab extends React.Component<ValidatioinTabProps, any> {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Tab {...this.props}>
                Validation Tab
            </Tab>
        );
    }
}

export default ValidatioinTab;

export interface ValidatioinTabProps extends TabItem {
}
