import * as React from 'react';
import { Tab } from 'react-bootstrap';
import { TabItem } from './';
import CustomTextArea from './_textAreaEditor';

class HTMLTab extends React.Component<TabItem, any> {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<Tab.Pane eventKey={this.props.eventKey}>
				<div className="form-group">
					<CustomTextArea field="properties.html" id="properties.html" mode={"html"}/>
				</div>
			</Tab.Pane>
		);
	}
}

export default HTMLTab;