import * as React from 'react';
import { Tab } from 'react-bootstrap';
import { TabItem } from './';
import { Text } from 'react-form';

class TextboxTab extends React.Component<TabItem, any> {
	constructor(props) {
		super(props);
	}
	render() {
		const {item} = this.props;
		return (
			<Tab.Pane {...this.props}>
				<div>
					{
						(item.type == "textField" || item.type == "quantity" || item.type == "dropdown") ?
							<div>
								<div className="form-group">
									<h4><label className="">Label Css</label></h4>
								</div>
								<div className="form-group">
									<label className="">Class</label>
									<Text className="form-control" field="properties.labelCss.baseClass"/>
								</div>
								<div className="form-group">
									<label className="">Style (example: color:blue;font-size:14px; )</label>
									<Text className="form-control" field="properties.labelCss.baseStyle" />
								</div>
								<hr />
								<div className="form-group">
									<h4><label className="">Control Css</label></h4>
								</div>
								<div className="form-group">
									<label className="">Class</label>
									<Text className="form-control" field="properties.controlCss.baseClass"/>
								</div>
								<div className="form-group">
									<label className="">Style (example: color:blue;font-size:14px; )</label>
									<Text className="form-control" field="properties.controlCss.baseStyle" />
								</div>
							</div>
						: (item.type == "addtocart") ?
							<div>
								<div className="form-group">
									<h4><label className="">Control Css</label></h4>
								</div>
								<div className="form-group">
									<label className="">Class</label>
									<Text className="form-control" field="properties.controlCss.baseClass"/>
								</div>
								<div className="form-group">
									<label className="">Style (example: color:blue;font-size:14px; )</label>
									<Text className="form-control" field="properties.controlCss.baseStyle" />
								</div>
							</div>
						: (item.type == "column") ?
							<div>
								<div className="form-group">
									<h4><label className="">Control Css</label></h4>
								</div>
								<div className="form-group">
									<label className="">First Column Class</label>
									<Text className="form-control" field="properties.columnCssFirst.baseClass"/>
								</div>
								<div className="form-group">
									<label className="">First Column Style</label>
									<Text className="form-control" field="properties.columnCssFirst.baseStyle" />
								</div>

								<div className="form-group">
									<label className="">Second Column Class</label>
									<Text className="form-control" field="properties.columnCssSecond.baseClass"/>
								</div>
								<div className="form-group">
									<label className="">Second Column Style</label>
									<Text className="form-control" field="properties.columnCssSecond.baseStyle" />
								</div>
							</div>
						: (item.type == "panel") ?
							<div>
								<div className="form-group">
									<h4><label className="">Control Css</label></h4>
								</div>
								<div className="form-group">
									<label className="">Panel Class</label>
									<Text className="form-control" field="properties.panelCss.baseClass"/>
								</div>
								<div className="form-group">
									<label className="">Panel Style</label>
									<Text className="form-control" field="properties.panelCss.baseStyle" />
								</div>
							</div>
						: null
					}
				</div>
			</Tab.Pane>
		);
	}
}

export default TextboxTab;

export interface TextboxTabProps extends TabItem {
}
