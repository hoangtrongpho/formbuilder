import * as React from 'react';
import { Tab } from 'react-bootstrap';
import { TabItem } from './';
//import { Text } from 'react-form';
import { Button } from 'react-bootstrap';
import FormBuilderService from '../../../../services/formBuilderService';

class DropdownDataTab extends React.Component<DropdownDataTabProps, any> {
	constructor(props) {
		super(props);
		this.state = {
			isListChecked: true,
			dropValue: "",
			dropItem: "",
			dropObj: this.props.item.properties.dropdownData? this.props.item.properties.dropdownData: [{value:"0",item:"select one"}],
			resultData: this.props.item.properties?(this.props.item.properties.dropdownData?this.props.item.properties.dropdownData:""):"",

			connectionDropData:[{value:"0",item:"select one"}],
			connectionValue:"0",
			sqlString:"",
		};
	}
	render() {
		//const {item} = this.props;
		return (
			<Tab.Pane {...this.props}>
				<div>
					<div>
						<div className="form-group">
							<h4><label className="">Dropdown Data</label></h4>
							<div className="form-group">
								<div className="col-sm-6"><input name="dropdown-type" type="radio" value="List" checked={this.state.isListChecked} onChange={(e) => this.handleRadioChange(e)}/>List</div>
								<div className="col-sm-6"><input name="dropdown-type" type="radio" value="Query" checked={this.state.isListChecked?false:true} onChange={(e) => this.handleRadioChange(e)}/>Query</div>
							</div>
							<hr />
							{
								this.state.isListChecked? 
								<div>
									{/*================================================List Dropdown==============================================*/}
									<div className="form-group">
										<div className="col-sm-5"><input className="form-control" type="text" placeholder="Value" value={this.state.dropValue} onChange={(e) => {this.handleTextChange(e,"value")}}/></div>
										<div className="col-sm-5"><input className="form-control" type="text" placeholder="Item" value={this.state.dropItem}  onChange={(e) => {this.handleTextChange(e,"item")}}/></div>
										<div className="col-sm-2"><Button  onClick={this.addToList.bind(this)}>Add</Button></div>
									</div>
									<div className="form-group">
										<div className="col-sm-12">
											<table className="table">
												<thead>
													<tr>
														<th>value</th>
														<th>item</th>
														<th style={{textAlign:"center"}}>delete</th>
													</tr>
												</thead>
												<tbody>
													{this.state.dropObj.map((i: any, index) => {
														return 	<tr key={index}>
																	<td>{i.value}</td>
																	<td>{i.item}</td>
																	<td style={{textAlign:"center"}} onClick={(e) => {this.deleteDropdownObj(e,i)}}><Button>x</Button></td>
																</tr>
													})}
													
												</tbody>
											</table>
										</div>
									</div>
									<div className="form-group">
										<div className="col-sm-6">
											Result
										</div>
									</div>
									<div className="form-group">
										<div className="col-sm-6">
											<select defaultValue="0" className="form-control">
												{this.state.dropObj.map((i: any, index) => {
													return <option key={index} value={i.value}>{i.item}</option>;
												})}
											</select>
										</div>
									</div>
								</div>
								:
								<div>
									{/*================================================Query Dropdown==============================================*/}
									<div className="form-group">
										<div className="col-sm-6">
											Connection string
										</div>
									</div>
									<div className="form-group">
										<div className="col-sm-10">
											<select defaultValue="0" className="form-control" onChange={(e)=>this.handleConnectionChange(e)}>
												{
													this.state.connectionDropData.map((i: any, index) => {
													return <option key={index} value={i.value}>{i.item}</option>;
												})}
											</select>
										</div>
									</div>
									<div className="form-group">
										<div className="col-sm-8">
											 <textarea cols={60} rows={8} className="form-control" placeholder="SQL" value={this.state.sqlString} onChange={(e) => {this.handleTextChange(e,"sql")}}></textarea>
										</div>
										<div className="col-sm-2"><Button onClick={this.runQuery.bind(this)}>Run Query</Button></div>
									</div>
									<div className="form-group">
										<div className="col-sm-6">
											Result
										</div>
									</div>
									<div className="form-group">
										<div className="col-sm-6">
											<select defaultValue="0" className="form-control">
												{this.state.dropObj.map((i: any, index) => {
													return <option key={index} value={i.value}>{i.item}</option>;
												})}
											</select>
										</div>
									</div>
								</div>
							}
							<hr />
							<div style={{textAlign:"right"}}>
								<Button className="btn btn-danger" onClick={() => this.props.closeModal()}>Cancel</Button>&nbsp;&nbsp;
								<Button className="btn btn-success" onClick={this.savedata.bind(this)}>save</Button>
							</div>
						</div>
					</div>
				</div>
			</Tab.Pane>
		);
	}


	componentDidMount() {
		FormBuilderService.getConnections().then( (result:any) => {
			if (result)
				this.initConnectionDropdown(result);
		})
	}
	initConnectionDropdown(data){
		let connectionDropData=Array();
		connectionDropData.push({value:"0", item:"Select One"});
		data.forEach(element => {
			let _value= element;
			let _item=element.split(';')[0].split('=')[1];
			let obj= {value:_value, item:_item};
			connectionDropData.push(obj);
		});
		this.setState({connectionDropData:connectionDropData});
	}
	handleConnectionChange(e){
		this.state.connectionValue=e.target.value;
	}
	runQuery(){
		if(this.state.connectionValue=="0") {alert("Please Select Connection."); return;}
		if(this.state.sqlString=="") {alert("Please enter SQL."); return;}
		FormBuilderService.runDropdownQuery({
			sql: this.state.sqlString,
			connection:this.state.connectionValue
		}).then(result => {
			if(result != null){
				let jObject = JSON.parse(JSON.stringify(result));
				if(jObject.Columns.length != 2){
					alert("The SQL must return 2 fields. (*first is Value, second is Item)");
				}else{
					let valueKey = jObject.Columns[0];
					let itemKey = jObject.Columns[1];
					let resultData=Array();
					resultData.push({value:"0",item:"select one"});
					if(jObject.Data.length > 0){
						jObject.Data.forEach(element => {
							let obj ={value:element[valueKey],item:element[itemKey]};
							resultData.push(obj);
						});
						this.setState({dropObj:resultData});
						alert("Run Query Successfully.")
						return;
					}
					alert("The data is null at the moment.");
				}
			}else{
				alert ("Data is not available.");
			}	
			
		})
	}
	savedata(){
		let item = this.props.item;
		item.properties.dropdownData= this.state.dropObj;
		this.props.handleSubmit(item);
	}
	deleteDropdownObj(e,data){
		let array = [...this.state.dropObj];
		this.state.dropObj.forEach((element,index) => {
			if(element.value == data.value && element.item==data.item){
				array.splice(index, 1);
				this.setState({dropObj:array});
				//alert("deleted:"+index);
				//alert(JSON.stringify(array));
			}
		});
	}
	handleRadioChange(e){
		if(e.target.value == "List"){
			this.setState({isListChecked:true});
		}else{
			this.setState({isListChecked:false});
		}
	}
	handleTextChange(e,type){
		if(type=="value")
			this.setState({dropValue: e.target.value});
		else if(type=="item")
			this.setState({dropItem: e.target.value});
		else
			this.setState({sqlString: e.target.value});
	}
	addToList(){
		if(this.state.dropValue == "" || this.state.dropItem==""){
			alert("please fill value and item Textbox");
			return;
		}
		let obj={value:this.state.dropValue,item:this.state.dropItem};
		let newDropObj= this.state.dropObj;
		newDropObj.push(obj);
		this.state.dropValue=this.state.dropItem="";
		this.setState({dropObj:newDropObj});
	}
}

export default DropdownDataTab;

export interface DropdownDataTabProps extends TabItem {
	handleSubmit: Function;
	closeModal: Function;
}
