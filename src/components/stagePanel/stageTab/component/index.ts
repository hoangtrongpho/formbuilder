export interface TabItem {
	eventKey: string;
	title: string;
	nolabel?: string;
	item: any;
	formApi?:any;
}

export interface TabItemType {
	tabs: Array<string>;
	title: string,
	nolabel?: Array<string>
}

import ValidatioinTab from './validationTab';
import PropertiesTab from './propertiesTab';
import HTMLTab from './htmlTab';
import VisibilityTab from './visibilityTab';
import TextboxTab from './textboxTab';
import StyleTab from './styleTab';
import DropdownDataTab from './dropdownDataTab';
export {
	ValidatioinTab,
	PropertiesTab,
	HTMLTab,
	VisibilityTab,
	TextboxTab,
	StyleTab,
	DropdownDataTab
}