import * as React from 'react';
import { Tab } from 'react-bootstrap';
import { TabItem } from './';
// import { StageItemProperties } from '../../index';
import { Text, Select } from 'react-form';

const statusOptions = [
	{
		label: 'Top',
		value: 'top'
	},
	{
		label: 'Bottom',
		value: 'bottom'
	},
	{
		label: "Left",
		value: 'left'
	},
	{
		label: "Right",
		value: 'right'
	}
];

class PropertiesTab extends React.Component<TabItem, any> {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const nolabel = this.props.nolabel && this.props.nolabel.toLowerCase() === 'true' ? true : false;
		return (
			<Tab.Pane {...this.props}>
				<div className="form-group">
					<label className="">Name</label>
					<Text className="form-control" field="properties.name" id="properties.name" />
				</div>
				{nolabel ? (
					''
				) : (
						<div>
							<div className="form-group">
								<label className="">Label</label>
								<Text className="form-control" field="properties.label" id="properties.label" />
							</div>
							<div className="form-group">
								<label className="">Label Position</label>
								<Select className="form-control" field="properties.labelPosition" id="properties.labelPosition" options={statusOptions} />
							</div>
						</div>
					)}
			</Tab.Pane>
		);
	}
}

export default PropertiesTab;

export interface PropertiesTabProps extends TabItem {
}
