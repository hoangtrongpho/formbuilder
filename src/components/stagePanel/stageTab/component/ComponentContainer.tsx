import * as components from './index';
import * as React from 'react';

const BaseTab = (type, title, noLabel, item, formApi, rootZone, handleSubmit,closeModal) => {
	switch (type) {
		case 'validation':
			return (<components.ValidatioinTab 
				title={title} key={type} eventKey={type} nolabel={noLabel} item={item} />);
		case 'properties':
			return (<components.PropertiesTab 
				title={title} key={type} eventKey={type} nolabel={noLabel} item={item} />)
		case 'visibility':
			return (<components.VisibilityTab 
				title={title} key={type} eventKey={type} nolabel={noLabel} item={item} rootZone={rootZone}/>);
		case 'html':
			return (<components.HTMLTab
				title={title} key={type} eventKey={type} nolabel={noLabel} item={item} formApi={formApi} />);
		case 'textbox':
			return (<components.TextboxTab 
				title={title} key={type} eventKey={type} item={item} />)
		case 'style':
			return (<components.StyleTab 
				title={title} key={type} eventKey={type} item={item} />)
		case 'dropdownData':
			return (<components.DropdownDataTab 
				title={title} key={type} eventKey={type} item={item} handleSubmit={handleSubmit} closeModal={closeModal}/>)
	}

	return null;
}

export default BaseTab;