import * as React from 'react';
import { Tab } from 'react-bootstrap';
import { TabItem } from './';
import { RadioGroup, Radio, FormField } from 'react-form';
import { VisibilityRuleType } from '../../model/constants';
import QueryBuilder from 'react-querybuilder';
import CustomTextArea from './_textAreaEditor';
import { ZoneItem,ContainerItem,StageItem } from '../../index';
class VisibilityTab extends React.Component<VisibilityTabProps, any> {
    constructor(props) {
        super(props);
        if (this.props.item) {
            this.state = {
                ruleType: this.props.item.visibility.ruleType,
                ruleValue: this.props.item.visibility.ruleValue,
                rootZone: this.props.rootZone
            };
        } else {
            this.state = {
                ruleType: null,
                ruleValue: null,
                rootZone: this.props.rootZone
            };
        }
    }
    render() {
        return (
            <Tab {...this.props}>
                <div className="visibilityTab">
                    <RadioGroup field="visibility.ruleType" onChange={this.changeRuleType.bind(this)}>
                        {group => (
                            <div>
                                <label htmlFor="visibility.ruleType.RuleBuilder" className="radio-inline">
                                    <Radio group={group} value={VisibilityRuleType.RuleBuilder} id="visibility.ruleType.RuleBuilder" style={{ marginRight: '20px' }} /> RuleBuilder
                                </label>
                                <label htmlFor="visibility.ruleType.Javascript" className="radio-inline">
                                    <Radio group={group} value={VisibilityRuleType.Javascript} id="visibility.ruleType.Javascript" className="d-inline-block" /> Javascript
                                </label>
                            </div>
                        )}
                    </RadioGroup>
                    {this.state.ruleType == VisibilityRuleType.RuleBuilder ?
                        <CustomQueryBuilder rootZone={this.state.rootZone} field='visibility.ruleBuilder' id='visibility.ruleValue' /> :
                        this.state.ruleType == VisibilityRuleType.Javascript ? 
                        <div>
                            <label>Function must return true or false for if the object is visible or not</label>
                            <CustomTextArea field="visibility.javascript" id="visibility.javascript" mode={"javascript"}/>
                        </div> : null}
                </div>
            </Tab>
        );
    }

    changeRuleType(type) {
        let ruleValue = '';
        if (type === VisibilityRuleType.Javascript) {
            ruleValue = `function ${this.props.item.type}_${this.props.item.id}_FormBuilderOnVisible() {\n\t\n}`;
        }

        this.setState({
            ruleType: type,
            ruleValue: ruleValue
        });
    }
}

export default VisibilityTab;

export interface VisibilityTabProps extends TabItem {
    rootZone: ZoneItem;
}



class CustomQueryWrapper extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            rootZone: this.props.rootZone,
            ItemList : Array<ContainerItem >(),
            Fields: Array(),
        };
      
    }

    componentWillMount() {
        this._findAllStageItem(this.state.rootZone);
        if(this.state.ItemList.length)
            this._initField(this.state.ItemList);
    }
    //find all control in ZONE
    _findAllStageItem(zone: ZoneItem,container?: ContainerItem): null | { item: StageItem; index: number; zone: ZoneItem; container?: ContainerItem }{
        for (let i = 0; i < zone.children.length; i++) {
            let item = zone.children[i] as ContainerItem;
            if (item.isContainer) {
                if (item.zones && item.zones.length){
                    for (let j = 0; j < item.zones.length; j++) {
                        let result = this._findAllStageItem(item.zones[j], item);
                        if (result) return result;
                    }
                }
            }
            this.state.ItemList.push(item);
        }
        return null;
    }
    //init field from all Stage Items
    _initField(items:  Array<ContainerItem>){
        if(items.length>0){
            for(let i=0; i < items.length; i++){
                let obj ={
                    name:items[i].id, label:items[i].properties.label
                };
                this.state.Fields.push(obj);
            }
        }
        //alert(JSON.stringify(this.state.Fields));
    }
    render() {
        const {
        	fieldApi,
            onChange,
            ...rest
      	} = this.props;

        const {
            getValue,
            setValue,
            // setTouched
        } = fieldApi;

        // const fields = [
        //     { name: 'firstName', label: 'First Name' },
        //     { name: 'lastName', label: 'Last Name' },
        //     { name: 'age', label: 'Age' },
        //     { name: 'address', label: 'Address' },
        //     { name: 'phone', label: 'Phone' },
        //     { name: 'email', label: 'Email' },
        //     { name: 'twitter', label: 'Twitter' },
        //     { name: 'isDev', label: 'Is a Developer?', value: false },
        // ];

        return (
            <QueryBuilder fields={this.state.Fields} onQueryChange={(e) => {
                setValue(e);
                if (onChange) {
                    onChange(e);
                }
            }}
                query={getValue()}
                {...rest} />
        );
    }
}

const CustomQueryBuilder = FormField(CustomQueryWrapper);