import * as React from 'react';
import { Tab } from 'react-bootstrap';
import { TabItem } from './';
import { Select } from 'react-form';

const statusOptions = [
	{
		label: 'Phone Number (###) ###-####',
		value: 'phoneNumberFirst'
	},
	{
		label: 'Phone Number ###.###.####',
		value: 'phoneNumberSecond'
	},
	{
		label: 'Email',
		value: 'email'
	}
];

class TextboxTab extends React.Component<TabItem, any> {
	constructor(props) {
		super(props);
		this.state = {};
	}
	render() {
		return (
			<Tab.Pane {...this.props}>
				<div>
					<div className="form-group">
						<label className="">Format</label>
						<Select className="form-control" field="properties.textboxFormat" id="properties.textboxFormat" options={statusOptions} />
					</div>
				</div>
			</Tab.Pane>
		);
	}
}

export default TextboxTab;

export interface TextboxTabProps extends TabItem {
}
