import * as React from 'react';
import { ModalControlProps } from '../../modal/modalPanel';
// import StageTabControl from './stageTabControl';
import { Nav, Row, Col } from 'react-bootstrap';
import { StageItem } from '../index';
import { Button, Modal } from 'react-bootstrap';

class ModalXMPieEditControl extends React.Component<EditModelProps, any> {
  constructor(props, context) {
    super(props);
    this.state = {
      DocumentId: this.props.item.XMPieControl? (this.props.item.XMPieControl.DocumentId?this.props.item.XMPieControl.DocumentId:"341") : "341",
      Display: this.props.item.XMPieControl? (this.props.item.XMPieControl.Display?this.props.item.XMPieControl.Display:"Window") : "Window",
      RenderType:this.props.item.XMPieControl?(this.props.item.XMPieControl.RenderType?this.props.item.XMPieControl.RenderType:"JPG"):"JPG"
    };
  }

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.close} bsSize={'large'} dialogClassName={this.props.dialogClass}>
        <Modal.Header closeButton>
						<Modal.Title>XMPie Edit Control</Modal.Title>
				</Modal.Header>
        <Modal.Body>
        <Row className="clearfix">
          <Col sm={12}>
						<Nav bsStyle="pills" stacked>
              <Row className="clearfix" style={{paddingBottom:5}}>
                <Col sm={3}>Document Id:</Col>
                <Col sm={9}><input className="form-control" type="text" value={this.state.DocumentId} onChange={this.handleDocumentIdChange.bind(this)}/></Col>
              </Row>
              <Row className="clearfix" style={{paddingBottom:5}}>
                <Col sm={3}>Display:</Col>
                <Col sm={9}>
                  <select value={this.state.Display} onChange={this.handleDisplayChange.bind(this)}>
                    <option value="NewWindow">In New Window</option>
                    <option value="Window">In Window</option>
                    
                  </select>
                </Col>
              </Row>
              <Row className="clearfix">
                <Col sm={3}>Render Type:</Col>
                <Col sm={9}>
                  <select value={this.state.RenderType} onChange={this.handleRenderTypeChange.bind(this)}>
                    <option value="JPG">JPG</option>
                    <option value="PDF">PDF</option>
                  </select>
                </Col>
              </Row>
						</Nav>
					</Col>
        </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => this.props.close()} bsStyle={this.props.closeStyle}>
            Close
          </Button>
          <Button onClick={this.saveXMPieControl.bind(this)} bsStyle={this.props.saveStyle}>
              Save
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }

  // handleSubmit(form) {
  //   debugger
  //   this.props.handleSubmit(form);
  // }
  handleDisplayChange(event){
    this.setState({Display: event.target.value});
    //alert (event.target.value);
  }
  handleRenderTypeChange(event){
    this.setState({RenderType: event.target.value});
    //alert (event.target.value);
  }
  handleDocumentIdChange(event){
    this.setState({DocumentId: event.target.value});
    //alert (event.target.value);
  }
  saveXMPieControl(){
    this.props.item.XMPieControl.Display=this.state.Display;
    this.props.item.XMPieControl.RenderType=this.state.RenderType;
    this.props.item.XMPieControl.DocumentId=this.state.DocumentId;
    alert("Saved Successfully.");
    this.props.close();
    //this.props.item.data.
    //alert (JSON.stringify(JSON.parse(JSON.stringify(this.props.item)), null, 2));
  }
}

export interface EditModelProps extends ModalControlProps {
  item: StageItem;
  handleSubmit: Function;
}
export default ModalXMPieEditControl;