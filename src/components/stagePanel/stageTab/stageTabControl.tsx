import * as React from 'react';
import { Tab, Nav, NavItem, Row, Col } from 'react-bootstrap';
import { TabItemType } from './component';
import configs from './config';
import { StageItem, ZoneItem } from '../index';
import ComponentContainer from './component/ComponentContainer';

class StageTabControl extends React.Component<StageTabControlProps, any> {
	constructor(props) {
		super(props);
		this.state = {
			key: 'properties'
		};
	}
	handleSelect(key) {
		this.setState({ key });
	}
	_checkTab(e,tab){
		this.props.checkTab(tab);
	}
	render() {
		const keys = configs ? Object.keys(configs) : [];
		return (
			<Tab.Container id="left-stagetabs-control" defaultActiveKey={this.state.key}>
				<Row className="clearfix">
					<Col sm={3}>
						<Nav bsStyle="pills" stacked>
							{keys.map((k: string) => {
								let config = configs[k] as TabItemType;
								return (
									this.props.item && config.tabs.indexOf(this.props.item.type) < 0 ? null :
									<NavItem key={k} eventKey={k} onClick={(e) => this._checkTab(e,k)}>
										{configs[k].title}
									</NavItem>
								);
							})}
						</Nav>
					</Col>
					<Col sm={9}>
						<Tab.Content animation>
							{keys.map((k: string) => {
								let config = configs[k] as TabItemType;
								if (this.props.item && config.tabs.indexOf(this.props.item.type) < 0){
									return null;
								}
								let noLabel =
									config.nolabel && config.nolabel.indexOf(this.props.item.type) > -1
										? 'true'
										: 'false';
								let Component = ComponentContainer(k, config.title, noLabel, this.props.item, this.props.formApi, this.props.rootZone,this.props.handleSubmit,this.props.closeModal);
								return Component ? Component : null;
							})}
						</Tab.Content>
					</Col>
				</Row>
			</Tab.Container>
		);
	}
}

export default StageTabControl;

export interface StageTabControlProps {
	item: StageItem;
	rootZone: ZoneItem;
	formApi?:any;
	handleSubmit:Function;
	checkTab: Function;
	closeModal:Function;
}
