import { createActions, createReducer } from 'reduxsauce';
import * as Immutable from 'seamless-immutable';
import { StageItem } from './index';


/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  rootZoneUpdate: ['zoneItem']
})

export default Creators

export const INITIAL_STATE = Immutable({    
  rootZone: {
    containerId: '',
    index: 0,
    children: new Array<StageItem>()
  }
})

// the eagle has landed
export const root_zone_update = (state = INITIAL_STATE, action) => {
  return { ...state, rootZone: action.zoneItem};
}

// map our action types to our reducer functions
export const HANDLERS = {
  [Types.ROOT_ZONE_UPDATE]: root_zone_update
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
