import StagePanel from './stagePanel';
import * as Constants from './model/constants';
import Guid from 'guid';

export {
    StagePanel,
    Constants
}

export class StageItem {
    id: string;
    apiId?: string;
    type: string;
    isContainer?: boolean;
    containerId: string;
    zoneIndex: number;
    properties: StageItemProperties;
    visibility?: StageItemVisibility;  
    data?: any;  
    XMPieControl?:any;
    constructor(type: string) {
        let newGuid = Guid.raw().split('-');
        this.id = newGuid[newGuid.length - 1];
        this.type = type;
        this.properties = new StageItemProperties(this.id);
        this.visibility = new StageItemVisibility(this.type, this.id);
        //this.data={FileNames:"",imageUrl:"",Message:"",Status:"",DefaultImage:"",JobId:""};
        //this.XMPieControl={DocumentId:"341", Display:"Window", RenderType:"JPG"};
    }
}

export class StageItemProperties {
    label?: string;
    labelPosition?: string;
    textboxFormat?: string;
    labelCss?: BaseCss;
    controlCss?: BaseCss;
    columnCssFirst?: BaseCss;
    columnCssSecond?: BaseCss;
    panelCss?: BaseCss;
    dropdownData?: any;
    name: string;
    html?: string;
    constructor(name: string) {
        this.labelPosition = this.labelPosition ? this.labelPosition : 'top';
        this.name = name;
        this.label = "";
    }
}

export class BaseCss {
    baseClass: string;
    baseStyle: string;
}

export class StageItemVisibility {
    ruleType: string;
    ruleBuilder: string;
    javascript: string;
    constructor(type: string, id: string ) {
        this.javascript = `function ${type}_${id}_FormBuilderOnVisible() {\n\t\n}`;
    }
}

export interface ZoneItem {
    index: number; // Zone cannot be draggable, so its index cannot be changed 
    containerId: string;
    children: Array<StageItem>;
}

export interface ContainerItem extends StageItem {
    // zone:ZoneItem;
    zones: Array<ZoneItem>;
    // onDrop: (dragItem:StageItem, dropItem?:StageItem) => void;
}