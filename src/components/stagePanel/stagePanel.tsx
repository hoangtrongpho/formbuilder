import * as React from 'react';
import { Dialog } from '../../components/modal';
import { StageItem, Constants, ZoneItem, ContainerItem, BaseCss } from './';
import ContainerBase from './container/_base';
import Guid from 'guid';
import { FormMenuPanel } from '../../components/formMenu';
import { MenuBar } from '../../components/menuBar';
import ControlHelper from '../../libs/controlHelper';
import OnItemBind from '../../libs/onItemBind';
import ModalEditControl from './stageTab/modalEditControl';
import ModalXMPieEditControl from './stageTab/modalXMPieEditControl';
import { connect } from 'react-redux';
import Creators from './redux';
import { Button } from 'react-bootstrap';
import FormBuilderService from '../../services/formBuilderService';

const textArea = (value, onChange?) => {
	return (<textarea rows={20} style={{ width: '100%', resize: 'vertical' }} defaultValue={value} onChange={onChange} />);
};

class StagePanel extends React.Component<any, any> {
	private popupDialog: Dialog;
	private currentIndexInCollection: number = -1;
	constructor(props) {
		super(props);
		this.state = {
			XMPieEditItem: undefined,
			editItem: undefined,
			importValue: undefined,
			onWaiting: false,
			curTab:"normal",
		};
	}

	componentDidMount() {
		// this.props.stageUpdate(__DATA.formBuilderData);
		FormBuilderService.loadForm({
			productId:__DATA.productId
		}).then( (result:ZoneItem) => {
			if (result.children)
				this.props.stageUpdate(result);
		})
	}

	render() {
		const style = this.props.rootZone.children.length === 0 ? 'stage-panel empty' : 'stage-panel';
		return (
			<div className="container">
				<div className="form-toolbar pull-right">
					<MenuBar
						onImport={this.onImport.bind(this)}
						onExport={this.onExport.bind(this)}
						onCodeView={this.onCodeView.bind(this)}
						onPreview={this.onPreview.bind(this)}
						onSave={this.onSave.bind(this)}
						// onLoad={this.onLoad.bind(this)}
					/>
				</div>
				<div className="form-builder-panel">
					
					<FormMenuPanel />
					<div className={style}>
						{(this.state.onWaiting) ? 
							<div style={{position:"absolute",left:"50%",zIndex:100}}>
								<img style={{position:"relative",left:"-50%"}} src={"https://k61.kn3.net/taringa/1/5/6/4/3/3/34/adriano034/944.gif"}/>
							</div>
							: null}
						
						
						<Dialog
							ref={(popupDialog: Dialog) => {
								this.popupDialog = popupDialog;
							}}
						/>
						{this.state.editItem ? (
							<ModalEditControl
								curTab={this.state.curTab}
								checkTab={this._checkTab.bind(this)}
								handleSubmit={this.saveEditStageItem.bind(this)}
								show={this.state.editItem != undefined}
								close={this.onCloseModal.bind(this)}
								saveButton={(<Button type={'submit'} bsStyle={'success'}>Save</Button>)}
								item={this.state.editItem}
								rootZone = {this.props.rootZone}
							/>
						) : null}
						{/* Modal edit XMPie control */}
						{this.state.XMPieEditItem ? (
							<ModalXMPieEditControl
								handleSubmit={this.saveEditStageItem.bind(this)}
								show={this.state.XMPieEditItem != undefined}
								close={this.onCloseModal.bind(this)}
								saveButton={(<Button type={'submit'} bsStyle={'success'}>Save</Button>)}
								item={this.state.XMPieEditItem}
							/>
						) : null}
						<ContainerBase
							styles={{ width: '100%', minHeight: '500px' }}
							item={this.props.rootZone}
							events={{
								onDrop: this.onDrop.bind(this),
								onHover: this.onHover.bind(this),
								onEdit: this.onEdit.bind(this),
								onDelete: this.onDelete.bind(this),
								onCopy: this.onCopy.bind(this),
								onDone: this.onDone.bind(this)
							}}
						/>
					</div>
				</div>
			</div>
		);
	}
	_checkTab(tab){
		//alert(tab);
		this.setState({curTab:tab});
	}
	onDone(){
		if(this.state.onWaiting){
			this.setState({onWaiting:false});
		}
		else{
			let rootZone = this.props.rootZone as ZoneItem;
			let item: StageItem = new StageItem("textField");
			item.isContainer = false;
			item.containerId = "";
			item.zoneIndex = 0;
			rootZone.children.splice(0, 0, item);
			this.props.stageUpdate(rootZone);
			rootZone.children.shift();
			this.props.stageUpdate(rootZone);
			this.setState({onWaiting:true});
		}	
	}

	onHover(index: number) {
		this.currentIndexInCollection = index;
	}

	onDrop(source: StageItem, dest: ZoneItem | StageItem) {
		if (!source.id) {
			this._addItemToStage(source, dest as ZoneItem);
		} else {
			this._reOrder(source, dest as StageItem);
		}
	}

	onDelete(item) {
		let stagePanel = this;
		this.popupDialog.confirm('Are you sure you want to remove this field?', {
			saveStyle: 'danger',
			save: () => {
				stagePanel._removeItem(stagePanel, item);
			}
		});
	}

	onCopy(item: any) {
		let rootZone = this.props.rootZone;
		let cloneItem = this._cloneItem(item);
		let zoneIndex =
			(cloneItem as ZoneItem).index === undefined
				? (cloneItem as StageItem).zoneIndex
				: (cloneItem as ZoneItem).index;

		if (!cloneItem.containerId) {
			this._insertItemToCollection(rootZone.children, cloneItem);
		} else {
			let search = this._findStageItem(rootZone, cloneItem.containerId);
			if (!search) {
				console.error('Cannot find container in drop zone: ', cloneItem);
			} else {
				this._insertItemToCollection((search.item as ContainerItem).zones[zoneIndex].children, cloneItem);
			}
		}

		this.currentIndexInCollection = -1;
		this.props.stageUpdate(rootZone);
	}

	onEdit(item: StageItem) {
		if(item.type=="XMPie"){
			this.setState({XMPieEditItem: item});
		}else{
			this.setState({editItem: item});
		}
	}

	onCloseModal() {
		this.setState({editItem: undefined});
		this.setState({XMPieEditItem: undefined});
	}

	onSaveModal() {
		this.onCloseModal();
	}

	onImport() {
		let self = this;
		this.setState({ importValue: '' });
		this.popupDialog.confirm(
			textArea('', this._onSaveImport.bind(this)),
			{
				size: 'large',
				title: 'Import your form here...',
				hideHeader: false,
				saveStyle: 'success',
				saveText: 'Save',
				closeStyle: 'danger',
				closeText: 'Cancel',
				save: () => {
					let newValue = {
						containerId: '',
						index: 0,
						children: []
					};

					try {
						newValue = self.state.importValue ? JSON.parse(self.state.importValue) : newValue;
					} catch {
						console.error("invalid import data: ", self.state.importValue);
					}
					self.setState({ importValue: '' });
					this.props.stageUpdate(newValue);
				}
			}
		);
	}

	onExport() {
		// let form = JSON.stringify(JSON.parse(JSON.stringify(this.props.rootZone)), null, 2);
		let form =JSON.stringify(this.props.rootZone, null, 2);
		this.popupDialog.show(textArea(form), {
			size: 'large',
			title: 'Export your form here...',
			hideHeader: false
		});
	}

	onCodeView() {
		// let stagePanel = this;
		let form = ControlHelper.renderFormHtml(this.props.rootZone);
		this.popupDialog.show(textArea(form), {
			size: 'large',
			title: 'Rendered html...',
			hideHeader: false
		});
	}

	onPreview() {
		// alert('Preview - Under Contruction');
		let form = ControlHelper.renderFormHtml(this.props.rootZone);
		//temporay use localStore waiting to apply redux
		localStorage.setItem('previewForm', form);
		window.open('#/preview', '_blank');
	}

	onSave() {
		// let stagePanel = this;
		// let form = JSON.stringify(this.props.rootZone, null, 2);
		FormBuilderService.saveForm({
			productId: __DATA.productId,
			jsonContent: this.props.rootZone
		}).then(result => {
			alert ("Save success");
		})
		//alert(stringForm);
		// var data = JSON.stringify({
		// 	JsonContent: form
		// });
		// var xhr = new XMLHttpRequest();
		// xhr.open('POST', '/Plugins/CC.Plugins.FormBuilder/Main/SaveFormBuilder', true);
		// xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
		// xhr.send(data);
	}

	saveEditStageItem(item) {
		debugger;
		let rootZone = this.props.rootZone;
		let zoneIndex =
			(item as ZoneItem).index === undefined ? (item as StageItem).zoneIndex : (item as ZoneItem).index;
		if (!item.containerId) {
			this._updateItemToCollection(rootZone.children, item);
			this.props.stageUpdate(rootZone);
		} else {
			let search = this._findStageItem(rootZone, item.containerId);
			if (!search) {
				console.error('Cannot find container in drop zone: ', item);
			} else {
				this._updateItemToCollection((search.item as ContainerItem).zones[zoneIndex].children, item);
				this.props.stageUpdate(rootZone);
			}
		}
	}

	//#private Functions
	_reOrder(sourceItem: StageItem, destItem: any) {
		let rootZone = this.props.rootZone;
		let source = this._findStageItem(rootZone, sourceItem.id);
		let dest = this._findStageItem(rootZone, destItem.id ? destItem.id : destItem.containerId);
		let zoneIndex = destItem.id === undefined ? destItem.index : destItem.zoneIndex;

		if (source) {
			if (source.zone)
				source.zone.children.splice(source.index, 1); // remove source in collection
			else rootZone.children.splice(source.index, 1);

			if (!dest || !destItem.containerId) rootZone.children.splice(this.currentIndexInCollection, 0, sourceItem);
			else if (dest.item.isContainer) (dest.item as ContainerItem).zones[zoneIndex].children.push(sourceItem);
			else if (dest.zone) dest.zone.children.splice(this.currentIndexInCollection, 0, sourceItem);
		}

		this.currentIndexInCollection = -1;
		this.props.stageUpdate(rootZone);
	}

	// TODO: after moving a container to new position, MUST update all children (recursive) in itemIndexes.
	//    before removing a container from itemIndexes, should remove all children (not in real???)
	_addItemToStage(source: StageItem, dest: ZoneItem | StageItem) {
		let rootZone = this.props.rootZone;
		let zoneIndex =
			(dest as ZoneItem).index === undefined ? (dest as StageItem).zoneIndex : (dest as ZoneItem).index;

		let item: StageItem = new StageItem(source.type);
		item.isContainer = source.isContainer;
		item.containerId = dest.containerId;
		item.zoneIndex = zoneIndex;
		item.properties.label = OnItemBind.getLabelName(source,this.props.rootZone);
		this.initStageItemProp(item);
		if (source.isContainer) {
			let cols = 1;
			switch (source.type) {
				case Constants.StageItemType.COLUMN:
					cols = 2;
					let _style = "width:" + 100 / cols + '%';
					let baseCss = {
							baseStyle: _style,
							baseClass: "li-stage fb-column col-md-6"
						} as BaseCss
					item.properties.columnCssFirst = baseCss;
					item.properties.columnCssSecond = baseCss;
					break;
				case Constants.StageItemType.PANEL:
					cols = 1;
					let panelCss = {
							baseStyle: "width:100%",
							baseClass: "li-stage fb-column col-md-12"
						} as BaseCss
					item.properties.panelCss = panelCss;
					break;
			}

			item['zones'] = [];
			for (var i = 0; i < cols; i++) {
				item['zones'].push({
					containerId: item.id,
					index: i,
					children: []
				} as ZoneItem);
			}
		}

		// dropTargetId is NULL == dragging to root stage panel.
		if (!dest.containerId) {
			this._insertItemToCollection(rootZone.children, item);
		} else {
			let search = this._findStageItem(rootZone, dest.containerId);
			if (!search) {
				console.error('Cannot find container in drop zone: ', dest);
			} else {
				this._insertItemToCollection((search.item as ContainerItem).zones[zoneIndex].children, item);
			}
		}

		this.currentIndexInCollection = -1;
		this.props.stageUpdate(rootZone);
	}
	initStageItemProp(item){
		if(item.type=="XMPie"){
			item.data={FileNames:"",imageUrl:"",Message:"",Status:"",DefaultImage:"",JobId:""};
			item.XMPieControl = {DocumentId:"341", Display:"Window", RenderType:"JPG"};
		}else if(item.type=="dropdown"){
			item.properties.dropdownData=[{value:"0",item:"select one"}];
		}
	}
	_insertItemToCollection(collection: Array<StageItem>, item: StageItem) {
		if (this.currentIndexInCollection === -1) collection.push(item);
		else collection.splice(this.currentIndexInCollection, 0, item);
	}

	_updateItemToCollection(collection: Array<StageItem>, item: StageItem) {
		let index = collection.findIndex((v, i) => {
			return v.id === item.id;
		});
		let existName = collection.findIndex((v, i) => {
			return v.properties.name === item.properties.name && v.id !== item.id;
		});

		if (existName >= 0) {
			alert(item.properties.name + ' is existed');
			return;
		}

		if (index >= 0) {
			collection[index] = item;
			this.onCloseModal();
		}
	}

	_removeItemFromCollection(collection: Array<StageItem>, item: StageItem) {
		let index = collection.indexOf(item);
		collection.splice(index, 1);
	}

	_findStageItem(
		zone: ZoneItem,
		stageId: string,
		container?: ContainerItem
	): null | { item: StageItem; index: number; zone: ZoneItem; container?: ContainerItem } {
		for (let i = 0; i < zone.children.length; i++) {
			let item = zone.children[i] as ContainerItem;
			if (item.isContainer) {
				if (item.zones && item.zones.length)
					for (let j = 0; j < item.zones.length; j++) {
						let result = this._findStageItem(item.zones[j], stageId, item);
						if (
							result // Continue search in item.zones if not found.
						)
							return result;
					}
			}

			if (item.id === stageId) return { item, index: i, zone, container };
		}
		return null;
	}

	_cloneItem(item: any): any {
		//clone item without reference
		let newItem = JSON.parse(JSON.stringify(item));
		let newId = Guid.raw().split('-');
		newItem.id = newId[newId.length - 1];
		newItem.properties.name = newItem.id;

		if (newItem.isContainer && newItem['zones']) {
			newItem['zones'].forEach((z: ZoneItem) => {
				z.containerId = newItem.id;
				if (z.children.length > 0) {
					var tempArray: Array<any> = [];
					z.children.forEach((s) => {
						let child = this._cloneItem(s);
						child.containerId = z.containerId;
						tempArray.push(child);
					});
					z.children = tempArray;
				}
			});
		}

		return newItem;
	}

	_removeItem(stagePanel, item) {
		let rootZone = stagePanel.props.rootZone;
		let zoneIndex =
			(item as ZoneItem).index === undefined ? (item as StageItem).zoneIndex : (item as ZoneItem).index;
		if (!item.containerId) {
			stagePanel._removeItemFromCollection(rootZone.children, item);
		} else {
			let search = stagePanel._findStageItem(rootZone, item.containerId);
			if (!search) {
				console.error('Cannot find container in drop zone: ', item);
			} else {
				stagePanel._removeItemFromCollection((search.item as ContainerItem).zones[zoneIndex].children, item);
			}
		}

		stagePanel.props.stageUpdate(rootZone);
	}

	_onSaveImport(event) {
		this.setState({ importValue: event.target.value });
	}
}

const mapStateToProps = state => ({
	rootZone: JSON.parse(JSON.stringify(state.stage.rootZone)) // remove Immutable
})

const mapDispatchToProps = dispatch => ({
	stageUpdate: (zoneItem) => dispatch(Creators.rootZoneUpdate(zoneItem))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(StagePanel);

//export default withRouter(StagePanel);

export interface StageProps {
	// item: ZoneItem;
	editItem?: StageItem;
	importValue?: string;
}
