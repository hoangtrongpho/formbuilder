import * as React from 'react';
import { StageItem } from './';
import * as ReactDnd from 'react-dnd';
import { DragDropType } from '../../shared/common';
import * as Immutable from 'seamless-immutable';

class StageControl extends React.Component<StageControlProps, any> {
    constructor(props) {
		super(props); 
    }

	

    render() {
		const { connectDragSource, connectDropTarget, connectDragPreview, children, isOverCurrent } = this.props;
		const { onDelete, onEdit, onCopy } = this.props.events;
		let newStyle = this.props.item.properties.labelCss ? (this.props.item.properties.labelCss.baseStyle ? this.props.item.properties.labelCss.baseStyle : "") : "";
		let newClass = this.props.item.properties.labelCss ? (this.props.item.properties.labelCss.baseClass ? this.props.item.properties.labelCss.baseClass : "") : "";
		newClass = "field-label " + newClass;
		let styleobj={};
		if(newStyle != "")
			styleobj = this.getStyle(newStyle);

        return !connectDragSource || !connectDropTarget || !connectDragPreview ? null : connectDragPreview( 
            connectDropTarget(
				<div>
					<div>
					</div>
					{ !isOverCurrent ? null : 
						<div className='candidate-element'></div>
					}
					<div className='list-group-item'>
					{ <div className="tool-tip">
						{/* <a className="edit-button btn icon-pencil" onClick={ () => onDone(this.props.item) }></a> */}
						<a className="del-button btn icon-cancel" onClick={ () => onDelete(this.props.item) }></a>
						<a className="edit-button btn icon-pencil" onClick={ () => onEdit(this.props.item) }></a>
						<a className="copy-button btn icon-copy" onClick={ () => onCopy(this.props.item) }></a>
						{connectDragSource(<a className="move-button btn glyphicon glyphicon-align-justify"></a>)}
					</div>}
					{
						this.props.item.properties.labelPosition == 'top'  ?
							<div className="itemStyle">
								<div><label style={styleobj} className={newClass} >{this.props.item.properties.label}</label></div>
								<div>{ children }</div>								
							</div>: this.props.item.properties.labelPosition == 'bottom' ?
							<div className="itemStyle" style={{paddingTop:'20px'}}>
								<div>{ children }</div>	
								<div><label style={styleobj} className={newClass}>{this.props.item.properties.label}</label></div>							
							</div>: this.props.item.properties.labelPosition == 'right' ?
							<div className="itemStyle" style={{display:'flex',paddingTop:'20px',textAlign:'right'}}>
								<div style={{flexBasis:0,flexGrow:2,maxWidth:'100%'}}>{ children }</div>	
								<div style={{flexBasis:0,flexGrow:1,maxWidth:'100%'}}>
									<label style={styleobj} className={newClass}>{this.props.item.properties.label}</label>
								</div>					
							</div>: this.props.item.properties.labelPosition == 'left' ?
							<div className="itemStyle" style={{display:'flex',paddingTop:'20px'}}>
								<div style={{flexBasis:0,flexGrow:1,maxWidth:'100%'}}>
									<label style={styleobj} className={newClass}>{this.props.item.properties.label}</label>
								</div>	
								<div style={{flexBasis:0,flexGrow:2,maxWidth:'100%'}}>{ children }</div>					
							</div>:null
					}
						
					</div>
				</div>
            )
        );
    }
	
	getStyle(item){
        let sumObj = Array();
        if(item != ""){
            let styleArr = item.split(";");
            if(styleArr.length >0 ){
                for(let i=0;i<styleArr.length;i++)
                {
                    //alert(styleArr[i]);
                    let detailArr = styleArr[i].split(":");
                    let obj = {
                        name : detailArr[0],
                        value : detailArr[1]
                    };

                    sumObj.push(obj);
                }
            }
        }
        return this.transferToReactStyle(sumObj);
    }

    transferToReactStyle(item){
        let sumObj={};
        if(item.length >0){
            for(let i=0;i<item.length;i++){
                let _name = "";
                if(item[i].name.includes("-")){
                    let cssArr = item[i].name.split("-");
                    _name = cssArr[0]+this.capitalizeFirstLetter(cssArr[1]);
                }else{
                    _name = item[i].name;
                }

                if(_name != ""){
                     sumObj[_name]=item[i].value;
                }
            }
        }
        return sumObj;
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

const cardDragSource:ReactDnd.DragSourceSpec<StageControlProps> = {
	beginDrag(props) {
		return Immutable(props.item).asMutable();
	},

	// endDrag(props, monitor) {
	// 	if (!monitor) return;
	// 	debugger;
	// 	const droppedItem = monitor.getItem() as StageItem;
	// 	const didDrop = monitor.didDrop()

	// 	if (!didDrop) {
	// 		props.endDrag(droppedItem, props.item);
	// 	}
	// },
}

const cardDropTarget:ReactDnd.DropTargetSpec<StageControlProps>  = {
	drop(props, monitor) {
		if (!monitor) return props.item;
		const hasDroppedOnChild = monitor.didDrop();    
        // Don't handle the parent droppable item. Just focus on the deepest target.
		if (hasDroppedOnChild) 
			return;
			
		const dragItem = monitor.getItem() as StageItem; // Get from beginDrag.
        
        if (props.events.onDrop)
			props.events.onDrop(dragItem, props.item);
			
		return {};
	},

	hover(props, monitor) {
        if (!monitor) return;
		props.events.onHover(props.index);
	},
}

const dragSource = ReactDnd.DragSource(DragDropType.BOX, cardDragSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
	connectDragPreview: connect.dragPreview(),
}))(StageControl);

export default ReactDnd.DropTarget([DragDropType.BOX], cardDropTarget, (connect, monitor) => ({	
	connectDropTarget: connect.dropTarget(),
	isOverCurrent: monitor.isOver({ shallow: true }),
}))(dragSource)

export interface StageControlProps extends React.Props<StageControl>  {
    item:StageItem;
	children:React.ReactNode;
	index:number;
	events: StageControlEvents;
	
	connectDragPreview?: ReactDnd.ConnectDragPreview;
    connectDragSource?: ReactDnd.ConnectDragSource;
    connectDropTarget?: ReactDnd.ConnectDropTarget;
	
	isDragging?: boolean;
	isOverCurrent?: boolean;	
}

export interface StageControlEvents {
	onHover: Function;
	onDrop: Function;
	onDelete:Function;
	onEdit:Function;
	onCopy:Function;
	// onDone: Function;
}