import * as React from 'react';
import { StageItem, Constants, ZoneItem, ContainerItem } from '../';
import * as Container from '../container';
import * as Basic from '../basic';
import * as SmartStore from '../smartStore';
import { DragDropType } from '../../../shared/common';
import * as ReactDnd from 'react-dnd';
//import Guid from 'guid';

class ContainerBase extends React.Component<ContainerBaseProps, any> {
    constructor(props) {
        super(props);
    }

    render() {
        const { item, styles, isOverCurrent, connectDropTarget, events } = this.props;
        const placeHolderText = this.props.placeHolderText ? this.props.placeHolderText : "Drop Zone";
        return !connectDropTarget ? null : connectDropTarget(
            <div style={getListStyle(isOverCurrent, styles)} className={this.props.className}>
                {!item.children.length ?
                    <span>{placeHolderText}</span>
                    : item.children.map((value, index) => SwitchStageControl(value, index, events))}
            </div>
        );
    }
}

const getListStyle = (isDraggingOver, styles) => ({
    background: isDraggingOver ? 'lightblue' : 'white',
    // border: '1px solid #999',    
    // float: 'left',
    // margin: 5,    
    // minHeight: 50,
    ...styles
});

export const SwitchStageControl = (item: StageItem | ContainerItem, index: number, events: Container.ContainerEvents) => {
    switch (item.type) {
        //=================================  basic control  ======================================
        case Constants.StageItemType.TEXT_FIELD:
        {
            return (<Basic.TextField key={index}  item={item} index={index} events={events}/>)
        }
        case Constants.StageItemType.HTML:
        {
            return (<Basic.HTML key={index} item={item} index={index} events={events}/>)
        }
        //=================================  basic container  ======================================
        case Constants.StageItemType.COLUMN:
        {
            item.properties.name='Column';
            return (<Container.Column key={index} item={item as ContainerItem} index={index} events={events}/>)
        }
        case Constants.StageItemType.PANEL:
        {
            item.properties.name='Panel';
            return (<Container.Panel key={index} item={item as ContainerItem} index={index} events={events}/>)
        }
        //=================================  smart store control  ======================================
        case Constants.StageItemType.QUANTITY:
        {
            item.properties.name = 'Quantity';
            return (<SmartStore.Quantity key={index}  item={item} index={index} events={events}/>)
        }
        case Constants.StageItemType.ADDTOCART:
        {
            item.properties.name='';
            return (<SmartStore.AddToCart key={index} item={item} index={index} events={events}/>)
        }
        case Constants.StageItemType.XMPIE:
        {
            item.properties.name='';
            return (<SmartStore.XMPie key={index}  item={item} index={index} events={events}/>)
        }
        case Constants.StageItemType.VIEWPROOF:
        {
            item.properties.name='';
            return (<SmartStore.ViewProof key={index} item={item} index={index} events={events}/>)
        }
        case Constants.StageItemType.DROPDOWN:
        {
            return (<SmartStore.Dropdown key={index}  item={item} index={index} events={events}/>)
        }   
    }
    return (<div>Unknown control item type '{item.type}'</div>);
}

const boxTarget: ReactDnd.DropTargetSpec<ContainerBaseProps> = {
    drop(props, monitor, component) {
        if (!monitor) return;
        const hasDroppedOnChild = monitor.didDrop();
        // Don't handle the parent droppable item. Just focus on the deepest target.
        if (hasDroppedOnChild)
            return;

        const dragItem = monitor.getItem() as StageItem; // Get from beginDrag.
        // console.log("Drop props = ", props);

        if (props.events.onDrop)
            props.events.onDrop(dragItem, props.item);

        return {};
    },
}

export default ReactDnd.DropTarget(DragDropType.BOX, boxTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
}))(ContainerBase)

export interface ContainerBaseProps extends React.Props<ContainerBase> {
    // items?:  Immutable.ImmutableArray<StageItem>;
    item: ZoneItem;
    styles?: React.CSSProperties;
    className?:string;
    placeHolderText?: string;
    events: Container.ContainerEvents;    

    connectDropTarget?: ReactDnd.ConnectDragSource;
    isOver?: boolean;
    isOverCurrent?: boolean;
}


