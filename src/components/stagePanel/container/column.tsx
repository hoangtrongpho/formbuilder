import * as React from 'react';
import ContainerBase from './_base';
import { CommonContainerProps } from './';
import StageControl from '../stageControl';
import getStyle from '../basic/convertStyle';
// import { StageItem } from '../';
// import * as Immutable from 'seamless-immutable';

// const colStyles = {
//     float: 'left',
//     'width': '50%'
// };

class Column extends React.Component<ColumnProps, any> {
    constructor(props) {
        super(props);
    }

    render() {
        const { item, index, events } = this.props;
        //const width = 100 / item.zones.length + '%';

        let firstClass = this.props.item.properties.columnCssFirst ? (this.props.item.properties.columnCssFirst.baseClass ? this.props.item.properties.columnCssFirst.baseClass : "") : "";
        let newFirstStyle = this.props.item.properties.columnCssFirst ? (this.props.item.properties.columnCssFirst.baseStyle ? this.props.item.properties.columnCssFirst.baseStyle : "") : "";
		let firstStyleObj={};
		if(newFirstStyle != "")
			firstStyleObj = getStyle(newFirstStyle);

        let secondClass = this.props.item.properties.columnCssSecond ? (this.props.item.properties.columnCssSecond.baseClass ? this.props.item.properties.columnCssSecond.baseClass : "") : "";
        let newSecondStyle = this.props.item.properties.columnCssSecond ? (this.props.item.properties.columnCssSecond.baseStyle ? this.props.item.properties.columnCssSecond.baseStyle : "") : "";
		let secondStyleObj={};
		if(newFirstStyle != "")
			secondStyleObj = getStyle(newSecondStyle);
        return (
            <StageControl item={item} index={index} events={events}>                
                <div className="container-item prev-holder">
                    {item.zones.map((col, index) => 
                        {
                            if(index == 0){
                                return(
                                    <ContainerBase key={index} item={col} placeHolderText={"Column " + index} 
                                        className={firstClass} 
                                        styles={firstStyleObj}
                                        events={events}    
                                    />
                                )
                            }else{
                                return(
                                    <ContainerBase key={index} item={col} placeHolderText={"Column " + index} 
                                        className={secondClass} 
                                        styles={secondStyleObj}
                                        events={events}    
                                    />
                                )
                            }
                            
                        }
                        
                    )}
                </div>
            </StageControl>
        );
    }
}

export default Column;

export interface ColumnProps extends CommonContainerProps {
}

