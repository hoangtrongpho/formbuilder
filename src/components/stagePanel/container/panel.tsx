import * as React from 'react';
// import * as styles from '../../formControl/styles';
import ContainerBase from './_base';
import { CommonContainerProps } from './';
import StageControl from '../stageControl';
import getStyle from '../basic/convertStyle';

class Panel extends React.Component<PanelProps, any> {
    constructor(props) {
        super(props);
    }

    render() {
        const { item, index, events } = this.props;
        //const width = 100 / item.zones.length + '%';

        let panelClass = this.props.item.properties.panelCss ? (this.props.item.properties.panelCss.baseClass ? this.props.item.properties.panelCss.baseClass : "") : "";
        let newStyle = this.props.item.properties.panelCss ? (this.props.item.properties.panelCss.baseStyle ? this.props.item.properties.panelCss.baseStyle : "") : "";
		let firstStyleObj={};
		if(newStyle != "")
			firstStyleObj = getStyle(newStyle);

        return (
            <StageControl item={item} index={index} events={events}>                
                <div className="container-item prev-holder">
                    {item.zones.map((col, index) => (
                        <ContainerBase key={index} item={col} placeHolderText={"Column " + index}
                            className={panelClass} styles={firstStyleObj}
                            events={events}
                        />
                    ))}
                </div>
            </StageControl>
        );
    }
}

export default Panel;

export interface PanelProps extends CommonContainerProps {
}