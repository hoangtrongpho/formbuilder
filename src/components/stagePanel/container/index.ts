import Column from './column';
import Panel from './panel';
import { ContainerItem} from '../';

export { 
    Panel,
    Column,
}

export interface CommonContainerProps {
    item: ContainerItem;
    index:number;
    events: ContainerEvents;
}

export interface ContainerEvents {
    onDrop: Function;
    onHover: Function;
    onDelete: Function;
    onEdit: Function;
    onCopy: Function;
    onDone: Function;
}
