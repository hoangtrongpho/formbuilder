export const StageItemType = {
    TEXT_FIELD: "textField",
    COLUMN: "column",
    PANEL: "panel",
    HTML: "html",

    DROPDOWN: "dropdown",
    ADDTOCART: "addtocart",
    QUANTITY: "quantity",
    XMPIE:"XMPie",
    VIEWPROOF:"ViewProof"
}

export const LabelPosition = {
    TOP: 'top',
    BOTTOM: 'bottom',
    LEFT: 'left',
    RIGHT: 'right'
}

export const VisibilityRuleType = {
    Javascript: 'javascript',
    RuleBuilder: 'rulebuilder'
}