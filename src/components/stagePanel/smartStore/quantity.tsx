import { CommonBasicProps } from './'
import BaseElement from '../basic/_base';
import * as React from 'react';
import { StageItem } from '../index';
import getStyle from '../basic/convertStyle';

const renderChild = (item: StageItem,inputStyle: any,newClass:any) => {
    return (
        <div className="prev-holder">
            <div className="form-group">
                <input style={inputStyle} className={newClass} type="text" />
            </div>
        </div>
    );
}

class TextField extends React.Component<TextFieldProps, any> {
    constructor(props) {
        super(props);
    }

    render() {
        let newClass = this.props.item.properties.controlCss ? (this.props.item.properties.controlCss.baseClass ? this.props.item.properties.controlCss.baseClass : "form-control") : "form-control";
        let newStyle = this.props.item.properties.controlCss ? (this.props.item.properties.controlCss.baseStyle ? this.props.item.properties.controlCss.baseStyle : "") : "";
		let styleobj={};
		if(newStyle != "")
			styleobj = getStyle(newStyle);
        return (
            <BaseElement {...this.props} >
                {renderChild(this.props.item,styleobj,newClass)}
            </BaseElement>
        );
    }
}

export default TextField;

export interface TextFieldProps extends CommonBasicProps {
}