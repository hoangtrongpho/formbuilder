import { CommonBasicProps } from './'
import BaseElement from '../basic/_base';
import * as React from 'react';
import { connect } from 'react-redux';
import Creators from '../redux';
import {ZoneItem,ContainerItem,StageItem} from '../../stagePanel/';
import FormBuilderService from '../../../services/formBuilderService';
// import { StageItem, ContainerItem, ZoneItem } from '../../stagePanel/';
class ViewProof extends React.Component<ViewProofProps, any> {
    constructor(props) {
        super(props);
        this.state = {
            ItemList:Array<ContainerItem >(),
            reload:true
        };
    }
    // View Proof
    ViewProof(){
        //this.ReloadPage();
        const { onDone } = this.props.events;
        //show waiting icon
        onDone();
        let self =this;
        setTimeout(function() {
            //init XMPie ItemList exist in Zone
            self.setState({ItemList:new Array<ContainerItem >()});
            //find XMPie control in ZONE
            let rootZone = self.props.rootZone as ZoneItem;
            self._findXMPieStageItem(rootZone);
            //if have XMPie control in ZONE
            if(self.state.ItemList.length){
                //get document Id
                let DocumentId= self.state.ItemList[0].XMPieControl.DocumentId ? self.state.ItemList[0].XMPieControl.DocumentId : "";
                //alert("DocumentId 0: " + DocumentId);
                //=================================submit job to get JOBID===================================
                FormBuilderService.submitJob(DocumentId).then(result => {
                    var JsonObj=JSON.parse(JSON.stringify(result));
                    //check validation of Document ID 
                    if(JsonObj.JobID==null)
                    {
                        alert("Invalid Document.");
                        onDone();
                        return;
                    }
                    //alert("JsonObj.JobID:"+JsonObj.JobID);
                    self._getRealImage(JsonObj.JobID,rootZone);
                })// end submitJob
            }// end check XMPie exist
            else{
                alert("There is no XMPie item.");
                onDone();
            }
        }, 2000); 
        
    }
    _getRealImage(JobID,rootZone){
        const { onDone } = this.props.events;
        //=====================call getjob status by first XMPie Item ID=============
        FormBuilderService.getJobStatus(JobID).then(result => {
            debugger;
            var str =JSON.stringify(result);
            var newstr = str.replace(/\\/g, ""); 
            newstr = newstr.substr(1, newstr.length-2);
            var FJsonObj=JSON.parse(newstr);
            var ImgUrl = (FJsonObj.URLs.count>0 || FJsonObj.URLs)? FJsonObj.URLs : "";
            //===========================if have real URL==================
            if (ImgUrl.length !== 0){
                //alert(ImgUrl);
                //alert("ImgUrl/0:"+ImgUrl[0]);
                //if have real image > load to XMPie control
                //let search = this._updateXMPieImage(rootZone,ImgUrl[0]);
                let search = this._updateXMPieImage(rootZone,FJsonObj,JobID);
                if(search != null)
                {
                    //replace old item(default image) BY new item (real Image)
                    search.zone.children.splice(search.index, 1, search.item);
                    if(this.props.stageUpdate)
                    {
                        //update zone
                        this.props.stageUpdate(rootZone);
                    }
                }
                // RefreshZone to load new XMPie;
                let source: StageItem = new StageItem("textField");
                source.isContainer = false;
                source.containerId = "";
                source.zoneIndex = 0;
                let dest={children:[],containerId:"",index:0};
                this.RefreshZone(source, dest as ZoneItem);
                //clear waiting icon
                onDone();
            }else{
                this._getRealImage(JobID,rootZone);
            }
            
        });// end getJobStatus
    }
    // update XMPie Image
    _updateXMPieImage(zone: ZoneItem, FJsonObj, JobID){
        for (let i = 0; i < zone.children.length; i++) {
            let item = zone.children[i] as ContainerItem;
            if (item.isContainer) {
                if (item.zones && item.zones.length){
                    for (let j = 0; j < item.zones.length; j++) {
                        this._updateXMPieImage(item.zones[j], FJsonObj, JobID);
                        // if (
						// 	result // Continue search in item.zones if not found.
						// )
						// 	return result;
                    }
                }
            }
            //if (item.id === Id){
            if (item.type === "XMPie"){
                //UPDATE XMPie Item
                item.data.FileNames = FJsonObj.FileNames[0];
                item.data.imageUrl = FJsonObj.URLs[0];
                item.data.Message = FJsonObj.Message;
                item.data.Status = FJsonObj.Status;
                item.data.JobId = JobID;
                //alert("index:" + i + "/item.data.imageUrl" + JSON.stringify(item,null,2));
                return { item, index: i, zone };
            }
            
        }
        return null;
    }
    //find all XMPie control in ZONE
    _findXMPieStageItem(zone: ZoneItem,container?: ContainerItem): null | { item: StageItem; index: number; zone: ZoneItem; container?: ContainerItem }{

        for (let i = 0; i < zone.children.length; i++) {
            //debugger;
            let item = zone.children[i] as ContainerItem;
            if (item.isContainer) {
                if (item.zones && item.zones.length){
                    for (let j = 0; j < item.zones.length; j++) {
                        this._findXMPieStageItem(item.zones[j], item);
                        //if (result) return result;
                    }
                }
            }
       
            //let itemList = Array<ContainerItem>();
            if (item.type === "XMPie"){
                //debugger;
                this.state.ItemList.push(item);
                //itemList.push(item);
            }
            if (i === zone.children.length-1) {
                //alert("itemList:"+JSON.stringify(itemList,null,2));
                return { item, index: i, zone, container };
            }
            
        }
        return null;
    }
    // Refresh Zone
    RefreshZone(source: StageItem, dest: ZoneItem | StageItem) {
		let rootZone = this.props.rootZone as ZoneItem;
		let zoneIndex =
			(dest as ZoneItem).index === undefined ? (dest as StageItem).zoneIndex : (dest as ZoneItem).index;
		let item: StageItem = new StageItem(source.type);
		item.isContainer = source.isContainer;
		item.containerId = dest.containerId;
        item.zoneIndex = zoneIndex;
        rootZone.children.splice(0, 0, item);
		if(this.props.stageUpdate)
        {
            this.props.stageUpdate(rootZone);
            rootZone.children.shift();
            this.props.stageUpdate(rootZone);
        }
    }
    render() {
        return (
            <BaseElement {...this.props}>
                <div className="prev-holder">
                    <div className="form-group" style={{textAlign: "center"}}>
                        <button onClick={this.ViewProof.bind(this)} type="button" className="btn btn-primary">View Proof</button>
                    </div>
                </div>
            </BaseElement>
        );
    }
}

const mapStateToProps = (state: any) => ({
    rootZone: JSON.parse(JSON.stringify(state.stage.rootZone)), // remove Immutable
})
const mapDispatchToProps = dispatch => ({
	stageUpdate: (zoneItem) => dispatch(Creators.rootZoneUpdate(zoneItem))
})
// export default ViewProof;
export default connect<any,void, ViewProofProps>(
	mapStateToProps,mapDispatchToProps
)(ViewProof);
export interface ViewProofProps extends CommonBasicProps {
    rootZone?:ZoneItem;
    stageUpdate?: (zoneItem:ZoneItem) => void;
    events: StageControlEvents;
}
export interface StageControlEvents {
	onHover: Function;
	onDrop: Function;
	onDelete:Function;
	onEdit:Function;
	onCopy:Function;
	onDone: Function;
}