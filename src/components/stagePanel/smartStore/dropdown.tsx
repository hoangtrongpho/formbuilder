import { CommonBasicProps } from './'
import BaseElement from '../basic/_base';
import * as React from 'react';
import { StageItem } from '../index';
import getStyle from '../basic/convertStyle';

const renderChild = (item: StageItem,inputStyle: any,newClass:any) => {
    return (
        <div className="prev-holder">
            <div className="form-group">
                <select value={0} className={newClass} style={inputStyle}>
                    {
                        item.properties.dropdownData?
                        item.properties.dropdownData.map((i: any, index) => {return <option key={index} value={i.value}>{i.item}</option>;})
                        :<option value="0">Select One</option>
                    }
                </select>
            </div>
        </div>
    );
}

class Dropdown extends React.Component<DropdownProps, any> {
    constructor(props) {
        super(props);
    }
    render() {
        //alert(JSON.stringify(this.props.item,null,2))
        let newClass = this.props.item.properties.controlCss ? (this.props.item.properties.controlCss.baseClass ? this.props.item.properties.controlCss.baseClass : "form-control") : "form-control";
        let newStyle = this.props.item.properties.controlCss ? (this.props.item.properties.controlCss.baseStyle ? this.props.item.properties.controlCss.baseStyle : "") : "";
		let styleobj={};
		if(newStyle != "")
			styleobj = getStyle(newStyle);
        return (
            <BaseElement {...this.props} >
                {renderChild(this.props.item,styleobj,newClass)}
            </BaseElement>
        );
    }
}

export default Dropdown;

export interface DropdownProps extends CommonBasicProps {
}