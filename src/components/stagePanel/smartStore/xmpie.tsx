import { CommonBasicProps } from './'
import BaseElement from '../basic/_base';
import * as React from 'react';
import FormBuilderService from '../../../services/formBuilderService';
class XMPie extends React.Component<XMPieProps, any> {
    constructor(props) {
        super(props);
        this.state = {
            imageUrl: props.item.data ? props.item.data.imageUrl : null,
            DefaultImage: props.item.data ? props.item.data.DefaultImage : null
        };
    }

    componentWillMount(){
        var self = this;   
        if (!this.state.imageUrl) {
            FormBuilderService.submitJob("341").then(result => {
                var JsonObj=JSON.parse(JSON.stringify(result));
                let data = { imageUrl: "", DefaultImage : JsonObj.DefaultImage};
                self.props.item.data = data;
                self.setState({DefaultImage: JsonObj.DefaultImage});
            })
        }        
    }

    render() {
        const { imageUrl, DefaultImage } = this.state;
        return (
            <BaseElement {...this.props}>
                <div className="prev-holder">
                    <div className="form-group" style={{textAlign: "center"}}>
                        {(imageUrl && imageUrl!="") ? <img style={{width: '100%' }} src={imageUrl}/> : 
                            (DefaultImage!="") ? <img style={{width: '100%' }} src={DefaultImage} />: <label className="btn btn-primary">XMPie</label>
                        }
                        
                    </div>
                </div>
            </BaseElement>
        );
    }
}

export default XMPie;

export interface XMPieProps extends CommonBasicProps {
}