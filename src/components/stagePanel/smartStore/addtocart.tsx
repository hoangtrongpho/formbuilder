import { CommonBasicProps } from './'
import BaseElement from '../basic/_base';
import * as React from 'react';
import { StageItem } from '../index';
import getStyle from '../basic/convertStyle';

const renderChild = (item: StageItem,inputStyle: any,newClass:any) => {
    return (
        <div className="prev-holder">
            <div className="form-group">
                <button type="button" className={newClass} style={inputStyle}>Add To Card</button>
            </div>
        </div>
    );
}

class AddToCart extends React.Component<AddToCartProps, any> {
    constructor(props) {
        super(props);
    }

    render() {
        let newClass = this.props.item.properties.controlCss ? (this.props.item.properties.controlCss.baseClass ? this.props.item.properties.controlCss.baseClass : "btn btn-primary") : "btn btn-primary";
        let newStyle = this.props.item.properties.controlCss ? (this.props.item.properties.controlCss.baseStyle ? this.props.item.properties.controlCss.baseStyle : "") : "";
		let styleobj={};
		if(newStyle != "")
			styleobj = getStyle(newStyle);
        return (
            <BaseElement {...this.props} >
                {renderChild(this.props.item,styleobj,newClass)}
            </BaseElement>
        );
    }
}

export default AddToCart;

export interface AddToCartProps extends CommonBasicProps {
}