import Dropdown from './dropdown';
import AddToCart from './addtocart';
import ViewProof from './viewproof';
import XMPie from './xmpie';
import Quantity from './quantity';
import { StageItem } from '../';
import { ContainerEvents } from '../container';

export {
    Dropdown,
    AddToCart,
    ViewProof,
    XMPie,
    Quantity
}

export interface CommonBasicProps {
    item:StageItem;
    index:number;
    events:ContainerEvents;
}
