import * as React from 'react';
import { FormMenuItem } from './';
import * as Immutable from 'seamless-immutable';
import FormMenuControl from './formMenuControl';
import { Constants } from '../stagePanel';

class FormMenuPanel extends React.Component<any, {items: Immutable.ImmutableArray<FormMenuItem>;}> {
  constructor(props) {
    super(props);
    this.state = {
      items: Immutable([
        { type: Constants.StageItemType.TEXT_FIELD, label: "Text Field", icon:'icon-text' },
        { type: Constants.StageItemType.HTML, label: "HTML", icon:'glyphicon glyphicon-console' },
        { type: Constants.StageItemType.COLUMN, label: "Column", isContainer: true, icon:'glyphicon glyphicon-th-large' },
        { type: Constants.StageItemType.PANEL, label: "Panel", isContainer: true, icon:'icon-textarea' },


        { type: Constants.StageItemType.ADDTOCART, label: "Add To Cart", icon:'glyphicon glyphicon-shopping-cart' },
        { type: Constants.StageItemType.QUANTITY, label: "Quantity", icon:'glyphicon glyphicon-resize-vertical' },
        { type: Constants.StageItemType.XMPIE, label: "XMPie", icon:'glyphicon glyphicon-camera' },
        { type: Constants.StageItemType.VIEWPROOF, label: "View Proof", icon:'glyphicon glyphicon-eye-open' },
        { type: Constants.StageItemType.DROPDOWN, label: "Dropdown", icon:'glyphicon glyphicon-triangle-bottom' }
      ])
    };
  }
  
  render() {
    const { items } = this.state;
    return (      
        <div className='form-control-panel'>
          <ul className='control-list list-group ui-sortable'>
          {items.map( (item, index) => {
              if(item.type==Constants.StageItemType.TEXT_FIELD)
              {
                return (
                  <div key={index}>
                      <h4><span className="label label-default">Basic Controls</span></h4>
                      <li  key={index} className='sortable-control list-group-item'>
                      <FormMenuControl key={index} item={item} />  
                      </li>
                  </div>
                  
                )
              }
              else if(item.type==Constants.StageItemType.ADDTOCART)
              {
                return (
                  <div key={index} style={{marginTop:50}}>
                      <h4><span className="label label-default">Smart Store Controls</span></h4>
                      <li  key={index} className='sortable-control list-group-item'>
                      <FormMenuControl key={index} item={item} />  
                      </li>
                  </div>
                  
                )
              }else{
                return (
                  <li key={index} className='sortable-control list-group-item'>
                      <FormMenuControl key={index} item={item} />  
                  </li>
                )
              }
            })  
          }
          </ul>          
        </div>
    );
  }
}

export const listStyle = {
  background: 'lightgrey',
  padding: 8,
  width: 250,
  marginLeft: 100,
  float: 'left',
  minHeight: 150
};

export default FormMenuPanel;