import * as React from 'react';
import * as ReactDnd from 'react-dnd';
import { DragDropType } from '../../shared/common';
import { FormMenuItem } from './';
import * as Immutable from 'seamless-immutable';

const boxSource:ReactDnd.DragSourceSpec<FormMenuControlProps> = {
	beginDrag(props) {        
		return Immutable(props.item).asMutable();
	},
}

class FormMenuControl extends React.Component<FormMenuControlProps> {
    constructor(props, context) {
        super(props);        
    }

    render() {
        const { item, connectDragSource } = this.props;
        let className = item.icon + ' ui-sortable-handle';
        return !connectDragSource ? null : connectDragSource(
            <div>
                <span className={className}></span>  { item.label }
            </div>
        );
    }
}

export default ReactDnd.DragSource(DragDropType.BOX, boxSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
}))(FormMenuControl);

export interface FormMenuControlProps extends React.Props<FormMenuControl>  {
    item: FormMenuItem;

    connectDragSource?: ReactDnd.ConnectDragSource;
    connectDragPreview?: ReactDnd.ConnectDragPreview;
    isDragging?: boolean;
}