import FormMenuPanel from './formMenuPanel'

export { FormMenuPanel }

export interface FormMenuItem {
    type: string;
    label: string;
    isContainer?: boolean;
    icon: string;
}