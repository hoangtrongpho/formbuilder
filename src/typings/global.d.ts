interface JQuery {
    nestedSortable(options: any): JQuery;
    sortable(type:string):JQuery;
}

declare var __DATA:{
    productId: number,
    formBuilderData: any
};

declare module 'reduxsauce' {
    /**
     * 
     * @param config = {
     *  loginRequest: ['username', 'password'],
        loginSuccess: ['username'],
        loginFailure: ['error'],
        requestWithDefaultValues: { username: 'guest', password: null }
        logout: null,
        custom: (a, b) => ({ type: 'CUSTOM', total: a + b }) 
     * } 
     * @param options 
     */
    export function createActions(config:any, options?:DefaultOptions): {
        Types: { [index: string]: string },
        Creators: any
    };

    export function createTypes(types:string, options?:DefaultOptions):{ [index: string]: any };

    export function createReducer<T>(initialState:T, handler: { [index: string]: Function }): (state:T, action:any) => T;

    export interface DefaultOptions {
        prefix?:string
    }

    export function resettableReducer(types: string, originalReducer?: Function);
}