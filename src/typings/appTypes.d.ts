import { ImmutableObjectMixin } from 'seamless-immutable'

interface RawState  {
    auth: {
        token: {
            access_token:string;
            refresh_token:string;
        }
    };
    startup: {
        contact: {
            longlitude: string;
            latitude: string;
        }
    }
}

interface StartupState {

}


interface AppState extends ImmutableObjectMixin<RawState> { 
    mergeDeep<T>(a: T): T;
}
