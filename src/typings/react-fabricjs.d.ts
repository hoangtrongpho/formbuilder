declare module 'react-fabricjs' {
    class Canvas extends React.Component<CanvasProps, any> {  
    }
    class Circle extends React.Component<CircleProps, any> {  
    }
    class Path extends React.Component<PathProps, any> {  
    }
    class Text extends React.Component<TextProps, any> {  
    }
    export interface CanvasProps {
        width ?: string;
        height ?: string;
    }

    export interface CircleProps {
        radius ?: number;
        left ?: number;
        top ?: number;
        stroke ?: string;
    }
    export interface PathProps {
        path ?: string;
        fill ?: string;
        stroke ?: string;
        strokeWidth ?: number;
        opacity ?: number;
    }
    export interface TextProps {
        text ?: string;
        left ?: number;
        top ?: number;
        shadow ?: string;
        stroke ?: string;
        strokeWidth ?: number;
        fontStyle ?: string;
        fontFamily ?: string;
    }
}