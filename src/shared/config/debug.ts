

export default {    
    getAPI: true,            // should app hit API server for data? (Turn off if we're using codepush)
    init: () => {
      window["__DATA"] = {
        productId: 47,
        formBuilderData: {}
      }
    }
}
  