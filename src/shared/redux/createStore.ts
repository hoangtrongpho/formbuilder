import { createStore, compose, Reducer, applyMiddleware } from 'redux';
// import { autoRehydrate, persistStore } from 'redux-persist';
import { default as sagaMiddlewareFactory } from 'redux-saga';
// import RehydrationServices from '../Services/RehydrationServices'
// import ConfigPersist from '../config/reduxPersist';
// import ScreenTracking from './ScreenTrackingMiddleware'
// import StartupActions from '../redux/startupRedux';

// creates the store
export default (rootReducer:Reducer<any>, rootSaga:() => Iterator<any>) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = []
  const enhancers = <any>[]
  
  /* ------------- Analytics Middleware ------------- */
  // middleware.push(ScreenTracking)

  /* ------------- Saga Middleware ------------- */
  // const sagaMonitor = Config.useReactotron ? console.tron.createSagaMonitor() : null
  // const sagaMiddleware = sagaMiddlewareFactory({ sagaMonitor })
  const sagaMiddleware = sagaMiddlewareFactory();
  // middleware.push(sagaMiddleware);

  /* ------------- Assemble Middleware ------------- */
  
  enhancers.push(applyMiddleware(...middleware, sagaMiddleware))

  /* ------------- AutoRehydrate Enhancer ------------- */

  // add the autoRehydrate enhancer
  // if (ConfigPersist.active) {
  //   enhancers.push(autoRehydrate())
  // }

  const store = createStore(rootReducer, compose(...enhancers))
  
  // const startup = () => store.dispatch(StartupActions.startup())

  // // configure persistStore and check reducer version number
  // if (ConfigPersist.active) {
  //   // RehydrationServices.updateReducers(store)
  //   persistStore(store, ConfigPersist.storeConfig, startup).purge();
  // }

  // kick off root saga
  sagaMiddleware.run(rootSaga)

  return store
}
