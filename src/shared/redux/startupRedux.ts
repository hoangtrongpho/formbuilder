import { createActions, createReducer, createTypes } from 'reduxsauce'
import * as Immutable from 'seamless-immutable'
// import { AppState } from '../../typings/appTypes'


/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  startup: ['token'],  
  startupLoaded: null,
  updateLoadingStatus: ['status'],
  updateContact: ['data'],
})

export const StartupTypes = createTypes(`
    STARTUP
    STARTUP_LOADED
    UPDTE_LOADING_STATUS
    UPDATE_CONTACT
`)
export default Creators

export const INITIAL_STATE = Immutable({    
  loaded: false,
  screenLoading: false,
  contact: {
    longlitude: "151.2092037",
    latitude: "-33.8591322"
  }
})

export const updateContact = (state, { data }: {data:any}) => {  
  if (!data) data = {};
  return state.merge({ contact: data })
}

// Store API
export const updateLoadingStatus = (state:any, { status }: {status:boolean}) => {  
  // console.warn("Loading status: " + status);
  return state.merge({ screenLoading: status })
}

export const startupLoaded = (state:any) => {  
  return state.merge({ loaded: true })
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPDATE_LOADING_STATUS]: updateLoadingStatus,
  [Types.STARTUP_LOADED]: startupLoaded,
  [Types.UPDATE_CONTACT]: updateContact,
})
