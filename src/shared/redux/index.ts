import { combineReducers } from 'redux';
import configureStore from './createStore';
import rootSaga from '../sagas/';
import { reducer as formReducer } from 'redux-form';
import { reducer as stageReducer } from '../../components/stagePanel/redux';
import { resettableReducer } from 'reduxsauce';

// const navReducer = (state, action) => {
//   const newState = AppNavigation.router.getStateForAction(action, state)
//   return newState || state
// }
// listen for the action type of 'RESET', you can change this.
const resettable = resettableReducer('RESET');

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    // nav: navReducer,
    form: formReducer,
    // startup: require('./StartupRedux').reducer,
    // user: require('./UserRedux').reducer,
    // booking: require('./BookingRedux').reducer,
    // notifications: require('./NotificationRedux').reducer,
    stage: resettable(stageReducer),
  })

  return configureStore(rootReducer, rootSaga)
}