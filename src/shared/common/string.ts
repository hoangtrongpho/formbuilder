export const IsNullOrEmpty = (obj: string) => {
    if (typeof obj === 'string') {
        if (!obj || 0 === obj.length || /^\s*$/.test(obj)) {
            return true;
        } else {
            return false; // Not null or empty
        }
    }
    return true;
};