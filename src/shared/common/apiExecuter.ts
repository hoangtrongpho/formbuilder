const AUTHORIZATION_PREFIX = "bearer ";

export default class ApiExecuter {
    _baseUrl:string;
    // _urlRefreshToken;
    _isWebFormUnauthorized:boolean;
    _acquireRefreshToken:()=>Promise<any>;
    _accessToken:string;
    _refreshToken:string;
    _defaultHeaders = {};
    //constructor(baseUrl:string, urlRefreshToken:string) {
    constructor(baseUrl:string, acquireRefreshToken:()=>Promise<any>, isWebFormUnauthorized = false) {
        if (baseUrl.endsWith("/") === false)
            baseUrl = baseUrl + "/";
        this._baseUrl = baseUrl;  
        // this._urlRefreshToken = urlRefreshToken;      
        this._acquireRefreshToken = acquireRefreshToken;
        this._isWebFormUnauthorized = isWebFormUnauthorized;
        this._defaultHeaders = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
    }

    get(url, data?:{}) {
        return this.__fetchJsonResponse(url + "?" + (data ? this.__searchParams(data) : ""), {
            method: "GET",
            headers: this.__getJsonHeaderDefault()
        });
    }

    put(url, data?:{}) {
        return this.__fetchJsonResponse(url, {
            method: "PUT",
            headers: this.__getJsonHeaderDefault(),
            body: data ? JSON.stringify(data) : null
        });
    }

    delete(url, data?:{}) {
        return this.__fetchJsonResponse(url, {
            method: "DELETE",
            headers: this.__getJsonHeaderDefault(),
            body: data ? JSON.stringify(data) : null
        });
    }

    post(url, data?:{}) {
        //alert(JSON.stringify(data));
        return this.__fetchJsonResponse(url, {
            method: "POST",
            headers: this.__getJsonHeaderDefault(),
            body: data ? JSON.stringify(data) : null
        });
    }

    postForm(url, data?:{}, contentType = 'application/x-www-form-urlencoded') {
        return this.__fetchJsonResponse(url, {
            method: "POST",
            headers: {
                'Content-Type': contentType
            },
            body: data ? this.__searchParams(data) : null
        });
    }

    upload(url, files: Array<FileDto> | FileDto, data?:{}) {
        var form = new FormData();
        if (!files) throw new Error("Invalid 'files' input");

        if (!(<Array<FileDto>>files).length)
            form.append((<FileDto>files).apiParamName, (<FileDto>files).file);
        else
            for (let i=0; i < (<Array<FileDto>>files).length; i++) {
                form.append(files[i].apiParamName, files[i].file);                
            }        

        if (data)
            for (let name in data) {
                form.append(name, data[name]);
            }

        return this.__fetchJsonResponse(url, {
            method: 'POST',
            headers: this.__appendAuthHeader(),
            body: form
        })
    }

    download(url, data?:{}) {
        return fetch(this.__combineUrl(url), {
                method: 'GET',
                headers: this.__getJsonHeaderDefault(),
                body: data ? this.__searchParams(data) : null
            })
            .then(response => response.blob())
    }

    // downloadImage(url, data?:{}){
    //     return this.download(url, data)
    //             .then(blob => URL.createObjectURL(blob));
    // }

    setToken(accessToken:string, refreshToken:string) {
        this._accessToken = accessToken;
        this._refreshToken = refreshToken;
    }

    changeBaseUrl(baseUrl) {
        this._baseUrl = baseUrl;
    }

    setHeader(key:string, value:string) {
        if (key)
            this._defaultHeaders[key] = value;
    }

    /** @private */
    __fetchJsonResponse(url:string, init:RequestInit) {        
        let self = this;
        // init.credentials = "same-origin";
        init.credentials = "include";
        return new Promise( (resolve, reject) => {
            fetch(self.__combineUrl(url), init)
                .then( response => {
                    if (response["redirected"] || response.status === 401) {
                        if (self._acquireRefreshToken) {
                            self._acquireRefreshToken()
                                .then( token => {
                                    self.setToken(token.access_token, token.refresh_token);
                                    if (init.headers)
                                        init.headers["Authorization"] = AUTHORIZATION_PREFIX + this._accessToken;
                                    // re-fetch after refresh token.
                                    fetch(self.__combineUrl(url), init).then(response => response.json())
                                        .then(resultNext => self.__handleResult(resultNext, resolve, reject))
                                });
                        }
                        return undefined;
                    }
                    else
                        return response.text().then(function(text) {
                            try {
                                return text ? JSON.parse(text) : {}
                            }
                            catch(e) { 
                                // Parse json error. Mean that server did not return JSON
                                reject(text);
                            }
                        })
                })
                .then( (result: BaseResponse) => {                    
                    if (result)
                        self.__handleResult(result, resolve, reject);
                })
                // .catch( (reason) => {
                //     console.error(reason);
                //     reject(reason);
                // });
        });
    }

    /** @private */
    __handleResult(result:BaseResponse, resolve:(value?:{})=>void, reject:(reason?:any)=>void) {
        if (result.statusCode >= 500)
            reject(result.errors);
        else
            resolve(result);
    }

    /** @private */
    __getJsonHeaderDefault(withoutAuth = false) {           
        if (withoutAuth)
            return this._defaultHeaders;
        else
            return this.__appendAuthHeader(this._defaultHeaders);
    }

    /** @private */
    __appendAuthHeader(headers?:{}) {        
        if (!headers) headers = {};
        if (this._accessToken)
            headers["Authorization"] = AUTHORIZATION_PREFIX + this._accessToken;
        return headers;
    }

    /** @private */
    __searchParams(params) {
        return Object.keys(params).map((key) => {
            return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
        }).join('&');
    }

    /** @private */
    __combineUrl(url:string) {
        if (url.startsWith("http"))
            return url;

        if (url.startsWith("/"))
            url = url.substr(1);
        return this._baseUrl + url;
    }
}

export interface FileDto {
    apiParamName:string;
    file:File;
}

export interface BaseResponse {
    errors: Array<string>;
    payload: any;
    statusCode: number;
}