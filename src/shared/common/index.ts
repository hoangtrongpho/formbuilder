import * as Util from './util'
import * as String from './string'

export {
    Util,
    String
}

export const DragDropType = {
    BOX: 'box',
    // CARD: 'card'
}