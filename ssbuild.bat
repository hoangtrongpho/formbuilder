@echo off
set plugintFolder=..\Plugins\CC.Plugins.FormBuilder
set webPluginFolder=..\..\smartstorecore\src\Presentation\SmartStore.Web\Plugins\CC.Plugins.FormBuilder
set staticFolder=%plugintFolder%\Content\scripts\static\
IF EXIST %plugintFolder% (
    (robocopy .\build %plugintFolder%\Content\scripts /s /it) ^& if %ERRORLEVEL% leq 3 SET ERRORLEVEL = 0          
    REN %staticFolder%js\*.js.map main.js.map
    REN %staticFolder%js\*.js main.js
    REN %staticFolder%css\*.css.map main.css.map
    REN %staticFolder%css\*.css main.css

    IF EXIST %webPluginFolder% (
        (robocopy %plugintFolder%\Content\scripts %webPluginFolder%\Content\scripts /s /it) ^& if %ERRORLEVEL% leq 3 SET ERRORLEVEL = 0
    ) ELSE ( 
        echo Folder NOT FOUND: %webPluginFolder% 
    )
) ELSE ( 
    echo Folder NOT FOUND: %plugintFolder% 
)

